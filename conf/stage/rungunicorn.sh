#!/bin/bash
set -e
TIMEOUT=300   #to solve upload app package timeout issue

cd /home/ada/prod/fish/apps

# activate the virtualenv
source /opt/envs/fish/bin/activate

# used for web requests
exec python manage.py run_gunicorn -w 1 --bind=0.0.0.0:8001 --settings=settings.prod \
    --user=ada --group=ada \
    --timeout=$TIMEOUT \
    --log-level=info \
    --log-file=/home/ada/prod/fish/logs/fish.log 2>>/home/ada/prod/fish/logs/fish.log
