## linefish README

# 注: 若apps中某子模块要打包重用, 则将该子模块对应的static及templates目录直接置于模块目录内.

# 
python manage.py makemigrations
python manage.py migrate



## test


# 商店编号从 100100 开始
# 坐席登录用户名(username): <shop_id>_A009


## 各种自动化脚本放在这里

## 导出DB库数据, 包括create-info和data
mysqldump -u root -p fish > fish_201604102151.sql

## 只导出数据库的数据:
mysqldump -u root -p --no-create-info fish > fish_data_20160509.sql