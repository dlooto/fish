/**
 * Created by openlin on 2016/2/21.
 */
function AjaxSender(urlStr, methordStr, successFun, errorFun, jsonData) {
    $.ajax(
        {
            url: urlStr,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFTOKEN", $.cookie('csrftoken'));
                xhr.setRequestHeader("Authorization", $.cookie('authtoken'));
            },
            type: methordStr,
            data: jsonData,
            error: errorFun,
            success: successFun,
            dataType: 'json',
        });
}

// 显示消息提示框（有一个‘确定’按钮）
function show_dialog(title, content){
    $("#dialog_title").html(title);
    $("#dialog_content").html(content);
    $("#dialog").css('display','block');
}
function hide_dialog(){
    $("#dialog").css('display','none');
}
// 显示消息提示框（有一个‘确定’按钮）
function show_toast(content, time, callback){
    $("#toast_content").html(content);
    $("#toast").css('display','block');
    setTimeout(function () {
        $("#toast").css('display', 'none');
        if (callback != null){
            callback();
        }
    }, time);
}