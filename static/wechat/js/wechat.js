
function get_validate_code() {
    var phone = $("#phone").val();

    if (phone == ""){
        show_dialog('', '请填写手机号');
        return;
    }

    function failed(data) {
        if (data.status == 403){
            // 第一次post可能会出现csrftoken验证错误，故执行第二次请求
            get_validate_code();
        }
        else{
            show_dialog('', '网络故障');
        }
    }

    function succeed(data) {
        if (data.code == 1) {
            settime($("#btn_send_vcode"));
        }
        else{
            show_dialog('', data.code);
        }

    }
    var data = {
        "phone": phone,
        "scode": "$^0#lv$kycl5!d-hq0yp*wsx90oyt90n"
    }
    AjaxSender("/api/v1/users/check_phone", "post", succeed, failed, data);
}

function bind_phone() {
    var phone = $("#phone").val();
    var validate_code = $("#validate_code").val();
    if (phone == ""){
        show_dialog('', '请输入手机号');
        return;
    }
    if (validate_code == ""){
        show_dialog('', '请输入验证码');
        return;
    }

    function failed() {
        show_dialog('', '网络故障');
    }

    function succeed(data) {
        if (data.code == 1) {
            window.location.href = data.next;
        }
        else{
            show_dialog('', data.msg);
        }
    }

    var data = {
        "phone": phone,
        "validate_code": validate_code
    }
    AjaxSender(window.location, "post", succeed, failed, data);
}


function bind_phone_pub() {
    var phone = $("#phone").val();
    var validate_code = $("#validate_code").val();
    if (phone == ""){
        show_dialog('', '请输入手机号');
        return;
    }
    if (validate_code == ""){
        show_dialog('', '请输入验证码');
        return;
    }

    function failed() {
        show_dialog('', '网络故障');
    }

    function succeed(data) {
        if (data.code == 1) {
            show_toast("验证成功", 1500, function () {
                wx.closeWindow();
            })

        }
        else{
            show_dialog('', data.msg);
        }
    }

    var data = {
        "phone": phone,
        "validate_code": validate_code
    }
    AjaxSender(window.location, "post", succeed, failed, data);
}

function join_fish(){
    var type = $("#business_type").val();

    function failed() {
        show_dialog('', '网络故障');
    }

    function succeed(data) {
        if (data.code == 1) {
            window.location = '/fish/wechat/fish_detail' + window.location.search;
        }
        else{
            show_dialog('', data.msg);
        }
    }
    var data = {
        "busi_type": type,
    }
    var url = "/api/v1/fish/new_fish" + window.location.search;
    AjaxSender(url, "post", succeed, failed, data);
}
