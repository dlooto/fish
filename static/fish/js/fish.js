// fish js lib

// 获取短信验证码倒计时
var countdown = 60;
function settime(obj) {
    if (countdown == 0) { //可重新开始获取验证码
        obj.attr("disabled", false);
        obj.val("获取短信验证码");
        countdown = 60;
        return;
    } else {
        obj.attr("disabled", true);
        obj.val("验证码已发送(" + countdown + ")");
        countdown--;
    }

    setTimeout(function() { settime(obj) }, 1000);
}

// 页面表单提交后, 对结果进行后续处理. 可通用
var setResult = function(res, destUrl) {
    if (res.code == 20003) { // 密码未设置/账号未激活
        window.location.href = '/users/w/activate';  //跳转到列表页面
        return;
    }

    if (res.code != 1) { // error
        $("#set_result").css('color', '#ff7f48');
        $("#set_result").html('');
        $("#set_result").append(res.msg);
        return;
    }

    $("#set_result").css('color', '#009900');
    $("#set_result").html('');
    if(destUrl != '') {
        window.location.href = destUrl;  //跳转到列表页面
        return;
    }
}

// 判空
function checkNull(field, errMsg) {
    if(isNull(field)) {
        $("#set_result").css('color', 'red');
        $("#set_result").html('');
        $("#set_result").append(errMsg);
        return true;
    }
    return false;
}