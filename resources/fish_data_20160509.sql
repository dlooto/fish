-- MySQL dump 10.13  Distrib 5.5.29, for osx10.6 (i386)
--
-- Host: localhost    Database: fish
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add content type',3,'add_contenttype'),(8,'Can change content type',3,'change_contenttype'),(9,'Can delete content type',3,'delete_contenttype'),(10,'Can add session',4,'add_session'),(11,'Can change session',4,'change_session'),(12,'Can delete session',4,'delete_session'),(13,'Can add site',5,'add_site'),(14,'Can change site',5,'change_site'),(15,'Can delete site',5,'delete_site'),(16,'Can add log entry',6,'add_logentry'),(17,'Can change log entry',6,'change_logentry'),(18,'Can delete log entry',6,'delete_logentry'),(19,'Can add token',7,'add_token'),(20,'Can change token',7,'change_token'),(21,'Can delete token',7,'delete_token'),(22,'Can add 用户',8,'add_user'),(23,'Can change 用户',8,'change_user'),(24,'Can delete 用户',8,'delete_user'),(25,'Can add 重置密码的验证码',9,'add_passwordresetrecord'),(26,'Can change 重置密码的验证码',9,'change_passwordresetrecord'),(27,'Can delete 重置密码的验证码',9,'delete_passwordresetrecord'),(28,'Can add 手机绑定记录',10,'add_mobilebindingrecord'),(29,'Can change 手机绑定记录',10,'change_mobilebindingrecord'),(30,'Can delete 手机绑定记录',10,'delete_mobilebindingrecord'),(31,'Can add 第3方登录账号',11,'add_openaccount'),(32,'Can change 第3方登录账号',11,'change_openaccount'),(33,'Can delete 第3方登录账号',11,'delete_openaccount'),(34,'Can add 短信验证码',12,'add_coderecord'),(35,'Can change 短信验证码',12,'change_coderecord'),(36,'Can delete 短信验证码',12,'delete_coderecord'),(37,'Can add 机构(Organization)',13,'add_organization'),(38,'Can change 机构(Organization)',13,'change_organization'),(39,'Can delete 机构(Organization)',13,'delete_organization'),(40,'Can add 业务类型记录',14,'add_shopbusitype'),(41,'Can change 业务类型记录',14,'change_shopbusitype'),(42,'Can delete 业务类型记录',14,'delete_shopbusitype'),(43,'Can add 营业点/门店(Shop)',15,'add_shop'),(44,'Can change 营业点/门店(Shop)',15,'change_shop'),(45,'Can delete 营业点/门店(Shop)',15,'delete_shop'),(46,'Can add 坐席/业务员(Clerk)',16,'add_clerk'),(47,'Can change 坐席/业务员(Clerk)',16,'change_clerk'),(48,'Can delete 坐席/业务员(Clerk)',16,'delete_clerk'),(49,'Can add 客户(Customer)',17,'add_customer'),(50,'Can change 客户(Customer)',17,'change_customer'),(51,'Can delete 客户(Customer)',17,'delete_customer'),(52,'Can add 用户排号(Fish)',18,'add_queuefish'),(53,'Can change 用户排号(Fish)',18,'change_queuefish'),(54,'Can delete 用户排号(Fish)',18,'delete_queuefish');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `authtoken_token`
--

LOCK TABLES `authtoken_token` WRITE;
/*!40000 ALTER TABLE `authtoken_token` DISABLE KEYS */;
INSERT INTO `authtoken_token` VALUES ('44aca4596ab28694fa8127e9e562d213bc71dd06',16,'2016-04-27 21:30:07'),('5fc1756060588cfbe49a0f16a72550b7db4f5fdf',15,'2016-04-27 21:15:04'),('77cac9f9ad76f2f4290c6c54056e4c33eb1527d1',12,'2016-04-26 18:59:45'),('90edf493858cf40cdeea2f7c0abd8e51144c309e',11,'2016-04-26 17:57:52'),('9a73667c4e09fbd3c269f35f5068c70cb0953ab2',2,'2016-04-26 09:16:26'),('aa2f0f7bfd8d094c6eca6aa24e9b00bbe4545cb6',19,'2016-05-08 17:30:37'),('k12345676890',4,'2016-04-26 10:34:37'),('k12345676891',5,'2016-04-26 10:34:58'),('k12345676892',6,'2016-04-26 10:35:07');
/*!40000 ALTER TABLE `authtoken_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2016-04-26 09:18:50',1,13,'1','1',2,'已修改 auth_status 。'),(2,'2016-04-26 10:32:24',1,8,'4','test_c1',1,''),(3,'2016-04-26 10:32:49',1,8,'5','test_c2',1,''),(4,'2016-04-26 10:32:59',1,8,'5','test_c2',2,'没有字段被修改。'),(5,'2016-04-26 10:33:02',1,8,'6','test_c3',1,''),(6,'2016-04-26 10:34:37',1,7,'k12345676890','k12345676890',1,''),(7,'2016-04-26 10:34:58',1,7,'k12345676891','k12345676891',1,''),(8,'2016-04-26 10:35:07',1,7,'k12345676892','k12345676892',1,''),(9,'2016-04-26 11:43:24',1,8,'4','test_c1',2,'已修改 phone 。'),(10,'2016-04-26 11:43:41',1,8,'5','test_c2',2,'已修改 phone 。'),(11,'2016-04-26 11:44:02',1,8,'6','test_c3',2,'已修改 phone 。'),(12,'2016-04-26 17:45:18',1,8,'4','test_c1',2,'已修改 phone 。'),(13,'2016-04-26 17:45:25',1,8,'6','test_c3',2,'已修改 phone 。'),(14,'2016-04-26 17:48:46',1,8,'2','15982249075',2,'已修改 phone 。'),(15,'2016-04-26 17:57:05',1,8,'2','15982249075_1',2,'已修改 username 。'),(16,'2016-04-26 18:17:04',1,18,'22','22',3,''),(17,'2016-04-26 18:17:04',1,18,'21','21',3,''),(18,'2016-04-26 18:17:04',1,18,'20','20',3,''),(19,'2016-04-26 18:17:04',1,18,'19','19',3,''),(20,'2016-04-26 18:28:48',1,18,'13','13',3,''),(21,'2016-04-27 22:35:47',1,8,'15','Junn',2,'已修改 phone 。'),(22,'2016-04-27 22:37:38',1,8,'11','Junn',2,'已修改 password 。'),(23,'2016-04-27 22:51:29',1,8,'2','15982249075_1',2,'已修改 password 和 phone 。'),(24,'2016-04-27 22:51:38',1,8,'11','Junn',2,'已修改 password 和 phone 。'),(25,'2016-04-27 23:02:33',1,15,'2','2',1,''),(26,'2016-05-09 10:46:28',1,8,'3','1_C109',3,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'content type','contenttypes','contenttype'),(4,'session','sessions','session'),(5,'site','sites','site'),(6,'log entry','admin','logentry'),(7,'token','authtoken','token'),(8,'用户','users','user'),(9,'重置密码的验证码','users','passwordresetrecord'),(10,'手机绑定记录','users','mobilebindingrecord'),(11,'第3方登录账号','users','openaccount'),(12,'短信验证码','users','coderecord'),(13,'机构(Organization)','fish','organization'),(14,'业务类型记录','fish','shopbusitype'),(15,'营业点/门店(Shop)','fish','shop'),(16,'坐席/业务员(Clerk)','fish','clerk'),(17,'客户(Customer)','fish','customer'),(18,'用户排号(Fish)','fish','queuefish');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fish_clerk`
--

LOCK TABLES `fish_clerk` WRITE;
/*!40000 ALTER TABLE `fish_clerk` DISABLE KEYS */;
INSERT INTO `fish_clerk` VALUES (2,'2016-04-27 23:10:04',17,1,'A002','C109'),(3,'2016-04-27 23:10:24',18,2,'A003','1号店'),(4,'2016-05-08 18:21:56',20,3,'S110',''),(5,'2016-05-08 18:23:16',21,3,'S109','S109'),(6,'2016-05-08 18:24:46',22,3,'S108','');
/*!40000 ALTER TABLE `fish_clerk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fish_customer`
--

LOCK TABLES `fish_customer` WRITE;
/*!40000 ALTER TABLE `fish_customer` DISABLE KEYS */;
INSERT INTO `fish_customer` VALUES (1,'2016-04-26 17:57:52',11,'Junn',NULL),(2,'2016-04-26 18:59:45',12,'Opening',NULL),(3,'2016-04-27 21:12:13',15,'Junn',NULL),(4,'2016-04-27 21:29:40',16,'Opening',NULL);
/*!40000 ALTER TABLE `fish_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fish_organization`
--

LOCK TABLES `fish_organization` WRITE;
/*!40000 ALTER TABLE `fish_organization` DISABLE KEYS */;
INSERT INTO `fish_organization` VALUES (1,'2016-04-26 09:16:26',2,'省人民医院','医院','','',2),(2,'2016-05-08 17:30:37',19,'秦老师甜品','餐饮','','',2);
/*!40000 ALTER TABLE `fish_organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fish_queuefish`
--

LOCK TABLES `fish_queuefish` WRITE;
/*!40000 ALTER TABLE `fish_queuefish` DISABLE KEYS */;
INSERT INTO `fish_queuefish` VALUES (1,'2016-04-26 10:36:02',1,4,'A001','C109','A',2,'2016-04-26 11:36:29',0),(2,'2016-04-26 10:36:25',1,4,'A002','C109','A',2,'2016-04-26 11:39:12',0),(3,'2016-04-26 11:28:04',1,4,'A003','C109','A',2,'2016-04-26 11:41:19',0),(4,'2016-04-26 11:41:30',1,4,'A004','C109','A',2,'2016-04-26 11:41:43',0),(5,'2016-04-26 11:41:32',1,4,'A005','C109','A',2,'2016-04-26 11:44:16',0),(6,'2016-04-26 11:41:32',1,4,'A006','C109','A',2,'2016-04-26 11:45:44',0),(7,'2016-04-26 11:41:32',1,4,'A007','C109','A',2,'2016-04-26 11:47:35',0),(8,'2016-04-26 11:41:33',1,4,'A008','C109','A',2,'2016-04-26 11:47:49',0),(9,'2016-04-26 11:41:37',1,4,'A009','C109','A',2,'2016-04-26 11:51:18',0),(10,'2016-04-26 11:41:37',1,4,'A010','C109','A',2,'2016-04-26 18:24:42',0),(11,'2016-04-26 11:41:37',1,4,'A011','C109','A',2,'2016-04-26 18:25:12',0),(12,'2016-04-26 11:41:37',1,4,'A012','C109','A',2,'2016-04-26 18:26:26',0),(13,'2016-04-26 18:43:43',1,5,'A013','C109','A',2,'2016-04-26 18:43:43',0),(14,'2016-04-26 16:06:43',1,5,'A014','C109','A',2,'2016-04-26 18:41:59',0),(15,'2016-04-26 16:06:43',1,5,'A015','C109','A',2,'2016-04-26 18:40:02',0),(16,'2016-04-26 16:06:43',1,5,'A016','C109','A',2,'2016-04-26 18:30:27',0),(17,'2016-04-26 16:06:43',1,5,'A017','C109','A',2,'2016-04-26 18:26:52',0),(18,'2016-04-26 17:58:42',1,11,'A013','C109','A',2,'2016-04-26 18:43:47',0),(19,'2016-04-26 18:44:20',1,11,'A013','C109','A',2,'2016-04-26 18:44:20',0),(20,'2016-04-26 18:44:21',1,11,'A013','C109','A',2,'2016-04-26 18:44:21',0),(21,'2016-04-26 18:48:21',1,11,'A013','C109','A',2,'2016-04-26 18:48:21',0),(22,'2016-04-26 18:48:23',1,11,'A013','C109','A',2,'2016-04-26 18:48:23',0),(23,'2016-04-26 18:48:40',1,11,'A001','C109','A',2,'2016-04-26 19:02:18',0),(24,'2016-04-26 19:00:01',1,12,'A002','C109','A',2,'2016-04-26 19:02:47',0),(25,'2016-04-26 19:03:12',1,5,'A001','C109','A',2,'2016-04-26 19:03:55',0),(26,'2016-04-26 19:12:57',1,12,'A001','C109','A',3,NULL,1),(27,'2016-04-26 19:14:23',1,11,'A002','C109','A',2,'2016-04-26 19:15:46',0),(28,'2016-04-27 20:41:26',1,11,'A001','C109','A',2,'2016-04-27 20:44:54',0),(29,'2016-04-27 20:43:41',1,12,'A002','C109','A',2,'2016-04-27 20:46:42',0),(30,'2016-04-27 20:52:01',1,11,'A001','C109','A',2,'2016-04-27 20:52:17',0),(31,'2016-04-27 20:52:03',1,12,'A002','C109','A',2,'2016-04-27 20:55:08',0),(32,'2016-04-27 20:56:43',1,12,'A001','C109','A',2,'2016-04-27 20:56:57',0),(33,'2016-04-27 20:56:47',1,11,'A002','C109','A',3,NULL,1),(34,'2016-04-27 21:28:12',1,15,'A001','C109','A',2,'2016-05-04 14:24:02',0),(35,'2016-04-27 21:30:13',1,16,'A002','C109','A',2,'2016-05-04 14:24:40',0),(36,'2016-05-04 14:33:32',1,2,'A001','C109','A',2,'2016-05-04 14:34:59',0),(37,'2016-05-04 14:36:19',1,2,'A001','C109','A',2,'2016-05-04 14:36:28',0),(38,'2016-05-04 16:34:51',1,2,'A001','C109','A',2,'2016-05-04 16:35:07',0),(39,'2016-05-04 16:40:27',1,2,'A001','C109','A',2,'2016-05-04 16:42:22',0),(40,'2016-05-04 16:45:17',1,2,'A001','C109','A',2,'2016-05-04 16:45:39',0),(41,'2016-05-04 16:45:48',1,2,'A002','C109','A',2,'2016-05-04 16:46:00',0),(42,'2016-05-04 16:46:03',1,2,'A003','C109','A',2,'2016-05-04 16:46:09',0),(43,'2016-05-04 16:46:13',1,2,'A004','C109','A',2,'2016-05-04 16:46:27',0),(44,'2016-05-04 17:03:09',1,2,'A005','C109','A',2,'2016-05-04 17:03:19',0);
/*!40000 ALTER TABLE `fish_queuefish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fish_shop`
--

LOCK TABLES `fish_shop` WRITE;
/*!40000 ALTER TABLE `fish_shop` DISABLE KEYS */;
INSERT INTO `fish_shop` VALUES (1,'2016-04-26 09:16:26',NULL,1,'S100100','招行圣菲店',NULL,NULL),(2,'2016-04-27 23:02:33',NULL,1,'S100102','百思店','',''),(3,'2016-05-08 17:30:37',NULL,2,'S100100','untitled',NULL,NULL);
/*!40000 ALTER TABLE `fish_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fish_shopbusitype`
--

LOCK TABLES `fish_shopbusitype` WRITE;
/*!40000 ALTER TABLE `fish_shopbusitype` DISABLE KEYS */;
/*!40000 ALTER TABLE `fish_shopbusitype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_coderecord`
--

LOCK TABLES `users_coderecord` WRITE;
/*!40000 ALTER TABLE `users_coderecord` DISABLE KEYS */;
INSERT INTO `users_coderecord` VALUES (1,'2016-04-26 09:15:32','15982249075','6491',0),(2,'2016-04-26 17:43:19','15982249075','2813',0),(3,'2016-04-26 17:45:47','15982249075','8946',0),(4,'2016-04-26 17:48:50','15982249075','3510',0),(5,'2016-04-26 17:57:29','15982249075','0913',0),(6,'2016-04-26 18:59:31','13880042961','0586',0),(7,'2016-04-27 21:14:07','15982249075','9710',0),(8,'2016-04-27 21:15:10','15982249075','2314',0),(9,'2016-04-27 21:16:50','15982249075','3962',0),(10,'2016-04-27 21:17:52','15982249075','3184',0),(11,'2016-04-27 21:27:56','15982249075','4596',0),(12,'2016-04-27 21:29:56','13880042961','9348',0),(13,'2016-05-08 17:30:00','15198200286','3460',0),(14,'2016-05-09 11:01:14','13880042961','097261',1);
/*!40000 ALTER TABLE `users_coderecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_mobilebindingrecord`
--

LOCK TABLES `users_mobilebindingrecord` WRITE;
/*!40000 ALTER TABLE `users_mobilebindingrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_mobilebindingrecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_openaccount`
--

LOCK TABLES `users_openaccount` WRITE;
/*!40000 ALTER TABLE `users_openaccount` DISABLE KEYS */;
INSERT INTO `users_openaccount` VALUES (1,'2016-04-26 17:57:52',11,'wexin.qq.com','ollWzs_iE_lxcy-jnBMBpIiy3lII','OezXcEiiBSKSxW0eoylIeES0ZOuxfqQ3sWHiBvQefQ0Oq1Jn55LRYujasTg-p8RsimyaenrMHP_LtzfZONwzJr8OooGFs6YkLN92z0_KkKAHvWQWTdqkBcWQpd7iUSxJAS-uHYy41J_hL_s70fklWQ',7200),(2,'2016-04-26 18:59:45',12,'wexin.qq.com','ollWzs4sWx8yLWT7QsYZEa5AA6mI','OezXcEiiBSKSxW0eoylIeES0ZOuxfqQ3sWHiBvQefQ2AXXFtPmj_dkVCcpeXlRWjPQaUFmkQz-bJ9TqY9NQTwqCQxrnCVgfIQ4cOAj5Ev3ug0E-QeNtyn6WWZi8aNJeePa5psnTWWUwvPXXKOaDIcA',7200),(3,'2016-04-27 21:12:13',15,'wechat','o0uY9wC0HtGq0pYkoHaB7bdflL9M','OezXcEiiBSKSxW0eoylIePHwJCQm_UGNhCM8hg5uqKxB7B76ctuNElawSw_IDKNGzO449LLFCHWzf9Uz5FfYSmn_t5d-Sk_ckiCKJfsKvYTDRtIabxxDWU6DhmJd4MxENuRaqBEoB3Ee6JJcaAqh2w',7200),(4,'2016-04-27 21:29:40',16,'wechat','o0uY9wJ3XqxDL3R_z2JuHA0oQhGc','OezXcEiiBSKSxW0eoylIePHwJCQm_UGNhCM8hg5uqKx8Db2Rg3UC2p3KPwxgrrYlSuO9_p4Kv6Fl6RbFje9hZKXsgiBkZyRJuXEsFG7Xvd9zEwVPU6DaJqiCediBdqng6kIGmKrP74JbP8-47K1B1Q',7200);
/*!40000 ALTER TABLE `users_openaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_passwordresetrecord`
--

LOCK TABLES `users_passwordresetrecord` WRITE;
/*!40000 ALTER TABLE `users_passwordresetrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_passwordresetrecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_user`
--

LOCK TABLES `users_user` WRITE;
/*!40000 ALTER TABLE `users_user` DISABLE KEYS */;
INSERT INTO `users_user` VALUES (1,'pbkdf2_sha256$10000$2DE4oW4cfgqk$jFxgw9vny4Kgf48km+ZmKlwUf0cZFZCFgRHbVIdJdVE=','2016-05-09 10:41:57',1,'2016-04-26 09:15:03','admin','','',1,1,'2016-04-26 09:15:03','E','','','2016-04-26','U',0,NULL,'NU'),(2,'pbkdf2_sha256$10000$34ozuEtNuZPO$OM+2KdFGW4NPTgEF07BIlsk7t82CAoP4NhcpEAAx98s=','2016-05-09 09:58:08',0,'2016-04-26 09:16:26','15982249075_1','15982249075','',0,1,'2016-04-26 09:16:26','E','','','2016-04-26','U',26,'127.0.0.1','OA'),(4,'pbkdf2_sha256$10000$NzgionCPEkxB$fwnMcQtJWPssWbe1z/Ntefe46Arr1Gp7gB2D0vzs6RI=','2016-04-26 10:32:24',0,'2016-04-26 10:32:24','test_c1','','',0,1,'2016-04-26 10:32:24','E','','','2016-04-26','U',0,NULL,'SC'),(5,'pbkdf2_sha256$10000$Ok0G4EBEkDpK$mgxT/NggbedWmHTI69vsGEDHM5e7yZnUGEs5KpmNZw4=','2016-04-26 10:32:49',0,'2016-04-26 10:32:49','test_c2','','',0,1,'2016-04-26 10:32:49','E','','','2016-04-26','U',0,NULL,'SC'),(6,'pbkdf2_sha256$10000$5WFy57TH3rNB$juD0piDLYmPj+yn7gDX2QmdTXMsDQjDmxWTdVqcwylQ=','2016-04-26 10:33:02',0,'2016-04-26 10:33:02','test_c3','','',0,1,'2016-04-26 10:33:02','E','','','2016-04-26','U',0,NULL,'SC'),(11,'pbkdf2_sha256$10000$M43gzrtTVXcz$h49U18doBk2lr0MsDEpL4RyEWW1LEJEv52JBcxH3RWU=','2016-04-27 22:49:42',0,'2016-04-26 17:57:45','15982249075','','',0,1,'2016-04-26 17:57:45','O','573d9c630b9511e6afdf542696d375cf.jpg','Junn','2016-04-26','M',5,'127.0.0.1','NU'),(12,'!','2016-04-26 18:59:45',0,'2016-04-26 18:59:44','13880042961','','',0,1,'2016-04-26 18:59:44','O','fc8f9d400b9d11e6b322542696d375cf.jpg','Opening','2016-04-26','M',1,'192.168.1.100','NU'),(13,'pbkdf2_sha256$10000$kd5GoQLmFIaE$Y82y4P7OtcZYGuwVfG1Pr2zOYJkn5P6ByVeG5L9YTS8=','2016-04-27 21:05:12',0,'2016-04-27 21:05:12','ad1886420c7811e69ab7542696d375cf','','',0,1,'2016-04-27 21:05:12','E','','Junn','2016-04-27','M',0,NULL,'CU'),(14,'pbkdf2_sha256$10000$mGXCA0yNCS4b$uGwTd8hpmZ4lP1rlscsMuzAO4mroS5yM78nPNi0/fYw=','2016-04-27 21:10:29',0,'2016-04-27 21:10:29','6a36d4910c7911e6bb65542696d375cf','','',0,1,'2016-04-27 21:10:29','E','','Junn','2016-04-27','M',0,NULL,'CU'),(15,'pbkdf2_sha256$10000$k6DsyJK6t2pk$91e+BMLIUrfGl5d1JBhkJ3iFYHG+0lh8vN+tCrAZLCo=','2016-04-27 21:46:18',0,'2016-04-27 21:12:12','a7b842e10c7911e699f0542696d375cf','','',0,1,'2016-04-27 21:12:12','E','a81ec28f0c7911e68ec4542696d375cf.jpg','Junn','2016-04-27','M',6,'112.44.72.97, 117.175.88.49','CU'),(16,'pbkdf2_sha256$10000$lWPVlMcB1Ldy$mmf9ieelFrMnLM1S9OxRR/pUFbY/gVIDJRTxkQ7rdps=','2016-04-27 21:47:05',0,'2016-04-27 21:29:40','184362990c7c11e6ae84542696d375cf','','',0,1,'2016-04-27 21:29:40','E','1877b27a0c7c11e69f96542696d375cf.jpg','Opening','2016-04-27','M',3,'223.104.9.178, 182.131.10.29','CU'),(17,'pbkdf2_sha256$10000$kdCUOhTFmmHh$bznrKdxltvFNi5NQmMOyF2MAyAGt+TdrasONk+LFON4=','2016-04-27 23:10:04',0,'2016-04-27 23:10:04','1_A002','','',0,1,'2016-04-27 23:10:04','E','','','2016-04-27','U',0,NULL,'NU'),(18,'pbkdf2_sha256$10000$NekmAXZvoS4F$jecmx7RlFuo+2orH5kSc51dAJtIiLVpKgOPWWWLhpJs=','2016-04-27 23:10:24',0,'2016-04-27 23:10:24','2_A003','','',0,1,'2016-04-27 23:10:24','E','','','2016-04-27','U',0,NULL,'NU'),(19,'pbkdf2_sha256$10000$EZqP83BDoxwP$gidN9QODpevVKUWfE2z/w77G/XM/JXouiQFC3CW3YwA=','2016-05-08 18:06:25',0,'2016-05-08 17:30:37','15198200286','15198200286','',0,1,'2016-05-08 17:30:37','E','','','2016-05-08','U',3,'127.0.0.1','OA'),(20,'pbkdf2_sha256$10000$bmSYS7ENyJFa$RPKHZgwzB6A1oNPOYOeKK/dtWC89XHmPQnUrfzmizq0=','2016-05-08 18:21:55',0,'2016-05-08 18:21:56','3_S110','','',0,1,'2016-05-08 18:21:56','E','','','2016-05-08','U',0,NULL,'NU'),(21,'pbkdf2_sha256$10000$RGAuV64ClLpV$nxiL23DC7Vbp68LiO81At/rIGtjGKZbH312sa9uU8Do=','2016-05-08 18:23:16',0,'2016-05-08 18:23:16','3_S109','','',0,1,'2016-05-08 18:23:16','E','','','2016-05-08','U',0,NULL,'NU'),(22,'pbkdf2_sha256$10000$DgkYvzfIICJs$kuTDEUQnrpBUqxakrYNTYz5QTyDFu7ToFhoJaW3K2vU=','2016-05-08 18:24:46',0,'2016-05-08 18:24:46','3_S108','','',0,1,'2016-05-08 18:24:46','E','','','2016-05-08','U',0,NULL,'NU'),(24,'!','2016-05-09 11:01:14',0,'2016-05-09 11:01:14','4a94dc0a159211e686c6542696d375cf','13880042961','',0,0,'2016-05-09 11:01:14','E','','','2016-05-09','U',0,NULL,'NU');
/*!40000 ALTER TABLE `users_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_user_groups`
--

LOCK TABLES `users_user_groups` WRITE;
/*!40000 ALTER TABLE `users_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_user_user_permissions`
--

LOCK TABLES `users_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `users_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-09 11:30:10
