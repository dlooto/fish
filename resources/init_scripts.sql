
/* DB初始化脚本, 如建表, 建索引, 初始化数据, 字段处理等 */

/* create database */
CREATE DATABASE fish CHARACTER SET UTF8 COLLATE utf8_general_ci;

/* 自增脚本 */
alter table users_user AUTO_INCREMENT=20140717;
alter table fish_organization AUTO_INCREMENT=20140224;
alter table fish_shop AUTO_INCREMENT=20160423;
