#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/16
#

"""

"""
from django.utils.html import format_html
from models import Organization, Shop, Clerk, Customer, QueueFish, ShopBusiType, \
    ShopAuth
from django.contrib import admin
import logging

logs = logging.getLogger(__name__)


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('id', 'orgn_name', 'industry', 'address', 'contact', 'colored_auth_status', 'user', )
    list_display_links = ('id', 'orgn_name',)
    search_fields = ('id', 'orgn_name', 'contact')
    list_filter = ('auth_status',)

    # 定制fish_status
    def colored_auth_status(self, obj):
        color = '#12d012'
        if obj.auth_status == 1:  # 等待审核
            color = '#12d012'
        elif obj.auth_status == 2:  # 审核通过
            color = '#3d85c6'
        elif obj.fish_status == 3:  # 审核失败
            color = '#ff9900'
        return format_html('<span style="color: %s;">%s</span>' %
                           (color, obj.get_auth_status_display()))

    colored_auth_status.short_description = "认证状态"
    colored_auth_status.admin_order_field = "auth_status"

admin.site.register(Organization, OrganizationAdmin)


class ShopBusiTypeAdmin(admin.ModelAdmin):
    list_display = ('shop', 'busi_no', 'busi_name', 'is_valid')
    search_fields = ('id', 'shop_no', 'shop_name')
    list_filter = ('is_valid', )

admin.site.register(ShopBusiType, ShopBusiTypeAdmin)


class ShopAdmin(admin.ModelAdmin):
    list_display = ('id', 'shop_no', 'shop_name', 'organ_name', 'user', )
    search_fields = ('id', 'shop_no', 'shop_name')
    list_display_links = ('id', 'shop_no',)

    def organ_name(self, obj):
        return u'%s %s' % (obj.organization_id, obj.organization.orgn_name)

    organ_name.short_description = u'所属企业'
    organ_name.admin_order_field = "organization"

admin.site.register(Shop, ShopAdmin)


class ClerkAdmin(admin.ModelAdmin):
    list_display = ('id', 'clerk_no', 'clerk_name', 'user', 'get_shop_name', )
    search_fields = ('id', 'user', 'shop', 'clerk_no', 'clerk_name')

    def get_shop_name(self, obj):
        return u'%s %s' % (obj.shop_id, obj.shop.shop_name)

    get_shop_name.short_description = u'门店名称'
    get_shop_name.admin_order_field = "shop"

admin.site.register(Clerk, ClerkAdmin)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_user_id', 'name', 'phone')
    search_fields = ('id', 'name', 'phone')

    def get_user_id(self, obj):
        return u'%s' % obj.user_id

    get_user_id.short_description = u'关联用户id'
    get_user_id.admin_order_field = "user"
    # def colored_name(self):
    #     return format_html('<span style="color: #{0};">{1} {2}</span>',
    #                        self.color_code,
    #                        self.first_name,
    #                        self.last_name)
    # colored_name.allow_tags = True

admin.site.register(Customer, CustomerAdmin)


class QueueFishAdmin(admin.ModelAdmin):
    list_display = ('id', 'fish_no', 'shop', 'get_customer', 'clerk_no', 'busi_type',
                    'colored_status', 'called_time', 'is_valid', 'created_time')

    list_display_links = ('id', 'fish_no', )
    search_fields = ('id', 'shop', 'fish_no', 'clerk_no')
    list_filter = ('busi_type', 'fish_status', 'is_valid')

    def get_customer(self, obj):
        return u'%s %s' % (obj.customer.id, obj.customer.name)

    get_customer.short_description = u"Customer"
    get_customer.admin_order_field = "customer"

    # 定制fish_status
    def colored_status(self, obj):
        color = '#12d012'
        if obj.fish_status == 1:  # 等待中
            color = '#12d012'
        elif obj.fish_status == 2:  # 已叫号
            color = '#3d85c6'
        elif obj.fish_status == 3:  # 已过号
            color = '#ff9900'
        elif obj.fish_status == 8:  # 业务办理开始
            color = '#12d012'
        elif obj.fish_status == 9:  # 业务办理完成
            color = '#0000ff'
        elif obj.fish_status == 10:  # 业务办理未完成结束
            color = '#ff0000'
        return format_html('<span style="color: %s;">%s</span>' %
                           (color, obj.get_fish_status_display()))
    colored_status.short_description = "排队状态"
    colored_status.admin_order_field = "fish_status"

admin.site.register(QueueFish, QueueFishAdmin)


class ShopAuthAdmin(admin.ModelAdmin):
    list_display = ('key', 'get_shop_name', 'expires', 'is_valid', 'created_time')
    search_fields = ('key', 'get_shop_name',)
    fields = ['shop', 'key', 'expires', 'is_valid']

    def get_shop_name(self, obj):
        return u'%s %s' % (obj.shop_id, obj.shop.shop_name)

    get_shop_name.short_description = u'门店'
    get_shop_name.admin_order_field = "shop"

admin.site.register(ShopAuth, ShopAuthAdmin)
