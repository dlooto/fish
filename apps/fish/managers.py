#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
#

"""

"""
import string, logging

from core.models import BaseManager
from utils import times, eggs


logs = logging.getLogger(__name__)

class ShopManager(BaseManager):

    def create_shop(self, organ, shop_no, shop_name='', user=None, **extra_fields):
        shop = self.model(organization=organ, shop_no=shop_no, shop_name=shop_name,
                          user=user, **extra_fields)
        shop.save()
        shop.gen_key()
        return shop


class QueueFishManager(BaseManager):
    pass


class CustomerManager(BaseManager):
    pass


class OrganizationManager(BaseManager):

    def create_orgn(self, **kwargs):
        orgn = self.model(**kwargs)
        orgn.save()
        orgn.create_default_shop()
        return orgn

    def approve(self, orgn):
        """
        对机构进行审核通过
        :param orgn:
        :return:
        """
        orgn.auth_status = orgn.AUTH_APPROVED
        return True

class ShopAuthManager(BaseManager):

    def generate(self, shop, expires=times.after_days(30, times.now())):
        """
        为门店生成接入授权key, 默认30天有效
        :param shop:
        :param expires:  key在该时间之前有效
        :return: 生成的key对象
        """
        auth = self.model(shop=shop, expires=expires, is_valid=True)
        auth.save()
        return auth

    def renew(self, shop, expires=times.after_days(30, times.now())):
        """刷新原有auth对象, 更新key"""
        try:
            auth = self.get(shop=shop)
            auth.refresh()
            logs.debug('auth_key refreshed')
            return auth
        except self.model.DoesNotExist:
            logs.debug('new shop_auth obj')
            return self.generate(shop, expires)
        except Exception, e:
            logs.exception(e)
            return None













