#coding=utf-8
#
# Created on 2014-9-9, by Junn
#
from rest_framework.fields import Field

from core.serializers import BaseModelSerializer, CustomPaginationSerializer
from fish.models import Clerk, QueueFish
from rest_framework import serializers


class ClerkSerializer(BaseModelSerializer):
    # avatar = serializers.SerializerMethodField('get_avatar')
    shop_id = Field(source='shop_id')
    user_id = Field(source='user_id')

    class Meta:
        model = Clerk
        fields = ('id', 'shop_id', 'user_id', 'clerk_no', 'clerk_name',)
        
# class PagingClerkSerializer(CustomPaginationSerializer):
#
#     class Meta:
#         object_serializer_class = ClerkSerializer


class QueueFishSerializer(BaseModelSerializer):
    shop_id = Field(source='shop_id')
    customer_id = Field(source='customer_id')

    class Meta:
        model = QueueFish
        fields = ('id', 'shop_id', 'customer_id', 'fish_no', 'busi_type', 'clerk_no',
                  'fish_status', 'called_time', 'is_valid', 'created_time')

