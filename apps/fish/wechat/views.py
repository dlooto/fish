# coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by kylin, on 16-4-18
#

"""

"""
from django.views.decorators.csrf import csrf_exempt
from wechat_sdk.exceptions import ParseError
from wechat_sdk.messages import *

from fish.wechat.conf import *
from utils import http, eggs
import logging
from users.models import User, CodeRecord, OpenAccount
from fish.models import Customer, Shop, QueueFish, ShopAuth

logs = logging.getLogger(__name__)


WECHAT_INSTANCE = WechatBasic(
    token=WECHAT_ACCOUNT_CONF['token'],
    appid=WECHAT_ACCOUNT_CONF['appid'],
    appsecret=WECHAT_ACCOUNT_CONF['app_secret'],
)


# 检查url中的shop_id是否有效，且是否与key匹配
def check_key(view_fun):
    def _deco(request, *args, **kwargs):
        shop_id = request.GET.get('shop_id', '')
        key = request.GET.get('key', '')
        if not shop_id or not key:
            return http.template_response(request, 'wechat/sys_notice.html', {'msg': u'商家微信接入权限错误'})

        shop = Shop.objects.get_cached(shop_id)
        if not shop:  # 商户配置的菜单url传递来的shop_id不存在
            return http.template_response(request, 'wechat/sys_notice.html', {'msg': u'商家微信接入权限错误'})

        # 检查key值是否有效
        if not shop.check_secret_key(key):
            return http.template_response(request, 'wechat/sys_notice.html', {'msg': u'商家微信接入权限错误'})

        ret = view_fun(request, *args, **kwargs)

        return ret

    return _deco


@check_key
def index(request):
    """
    微信用户进入排队功能的主页面.
    """
    # 装饰器已检查过shop_id和key值，所以此处直接写get('shop_id')不用带默认值去防止出错
    shop_id = request.GET.get('shop_id')
    key = request.GET.get('key')

    # 因为所有视图函数均需要验证key但只有index视图微信端会将key带过来，所以重定向的所有url中需要将
    # url_params带过去，否则验证不通过
    # 此处特别的以‘;’隔开两个参数，而非‘&’。因为微信重定向redirect_uri中经实验只能传递一个参数，
    # 因此，对应的login视图需要特别的处理这两个参数
    # 否则与微信验证url中的其他参数冲突（微信严格检查参数格式）
    url_params = "shop_and_key=%s|%s" % (shop_id, key)
    shop = Shop.objects.get_cached(shop_id)
    # 未登录
    if not request.user.is_authenticated():  # 用户未登录则重定向到微信授权页面，授权成功后，跳转至login
        # 此过程重定向到login页，虽然与shop_id和key无关，但仍然需要传递过去，以备登录成功后重定向到主页时需要shop相关验证
        return http.redirect_response(
            WECHAT_URL_CONF['open_authorize'] % (WECHAT_ACCOUNT_CONF['appid'], AUTH_REDIRECT_URL, url_params)
        )

    user_fish = shop.get_user_fish_in_queue(request.user.get_customer())
    if not user_fish:  # queue中没有该用户（两种可能，1.未排队。2.正在办理或正在被呼叫）
        # 此条件为当前用户正在办理业务或正在被呼叫
        if request.user.get_customer() in shop.get_all_serving_customers():
            data = {'person': 0, 'time': 0, 'shop': shop, 'fish': None}
            return http.template_response(request, 'wechat/fish_detail.html', data)

        return http.template_response(request, 'wechat/index.html', {'shop': shop})

    url_params = "shop_id=%s&key=%s" % (shop_id, key)
    return http.redirect_response('/fish/wechat/fish_detail?%s' % url_params)


# 该过程仅与贯鱼后台相关。与shop无关无需验证key或shop_id
def wechat_login(request):
    """用户微信登录Fish平台. 微信服务器回调进入该请求

    :return
        正确, 则返回到验证手机号页面或排队页面.
        异常, 则抛出异常
    """
    shop_and_key = request.GET.get('shop_and_key')
    shop_and_key = shop_and_key.split('|')
    shop_id = shop_and_key[0]
    key = shop_and_key[1]
    url_params = 'shop_id=%s&key=%s' % (shop_id, key)

    code = request.GET.get('code', '')  # 微信授权后返回的code码
    url = WECHAT_URL_CONF['openid_info_url'] % (WECHAT_ACCOUNT_CONF['appid'], WECHAT_ACCOUNT_CONF['app_secret'], code)

    # ###### 请求open_id, access_token
    wechat_info = None
    try:
        wechat_info = eval(http.send_request(WECHAT_URL_CONF['wechat_api_host'], url, protocol='https'))
        if not wechat_info:
            raise Exception('请求微信信息结果为空')
    except Exception, e:
        logs.error('请求微信信息异常: opendid, access_token, expires_in')
        logs.exception(e)
        raise e

    openid = wechat_info['openid']

    try:
        open_account = OpenAccount.objects.get(openid=openid)     # TODO: 安全起见可能需要验证access_token
        if not open_account.user.get_customer().phone:
            return http.redirect_response('/fish/wechat/bind_phone?openid=%s&%s' % (openid, url_params))

        open_account.user.post_login(request)
        return http.redirect_response('/fish/wechat/index?%s' % url_params)
    except OpenAccount.DoesNotExist:
        try:
            _create_user_account(wechat_info)
            url = "/fish/wechat/bind_phone?openid=%s&%s" % (openid, url_params)
            return http.redirect_response(url)  # 重定向到验证手机号页面
        except Exception, e:
            logs.exception(e)
            raise e
    except Exception, e:
        logs.exception(e)
        raise e


# 该过程仅与贯鱼后台相关。与shop无关无需验证key或shop_id
def bind_phone(request):
    """微信web用户绑定手机号页面"""

    shop_id = request.GET.get('shop_id')
    key = request.GET.get('key')
    url_params = 'shop_id=%s&key=%s' % (shop_id, key)

    if request.method == "GET":
        return http.template_response(request, 'wechat/bind_phone.html', {})

    if request.method == "POST":  # 执行OpenAccount注册并登录
        phone = request.POST.get('phone', None)
        validate_code = request.POST.get('validate_code', None)
        if not phone or not validate_code:
            return http.failed('手机号或验证码无效')

        if not CodeRecord.objects.check_code(phone, validate_code):
            return http.failed('验证码无效')

        try:
            openid = request.GET.get('openid', None)
            open_account = OpenAccount.objects.get(openid=openid)
            user = open_account.user

            # 将手机号写入Customer
            customer = user.get_customer()
            customer.phone = phone
            customer.save()

            user.post_login(request)
            token = user.get_authtoken()
            if not token:
                return http.resp('authtoken_error', 'Auth error')
            response = http.ok(data={'next': '/fish/wechat/index?%s' % url_params})
            response.set_cookie('authtoken', token)
            return response
        except Exception, e:
            logs.exception(e)
            return http.failed('手机号验证异常')


@check_key
def fish_detail(request):
    """用户已排队信息显示"""

    shop_id = request.GET.get('shop_id')
    key = request.GET.get('key')
    url_params = 'shop_id=%s&key=%s' % (shop_id, key)
    if not request.user.is_authenticated():
        return http.redirect_response('/fish/wechat/index?%s' % url_params)

    shop = Shop.objects.get_cached(shop_id)
    user_fish = shop.get_user_fish_in_queue(request.user.get_customer())
    if not user_fish:  # queue中没有该用户（两种可能，1.未排队。2.正在办理或正在被呼叫）
        # 此条件为当前用户正在办理业务或正在被呼叫
        if not request.user.get_customer() in shop.get_all_serving_customers():  # 未排队
            return http.template_response(request, 'wechat/index.html', {'shop': shop})

        data = {'person': 0, 'time': 0, 'shop': shop, 'fish': None}
        return http.template_response(request, 'wechat/fish_detail.html', data)

    person, time = shop.query_fish_status_in_queue(user_fish)
    data = {'person': person, 'time': time, 'shop': shop, 'fish': user_fish}
    return http.template_response(request, 'wechat/fish_detail.html', data)


# --------------------------------------------------非试图函数
def _create_user_account(wechat_info):
    """ ###### 在Fish平台创建用户账号(user, openAccount, customer) """

    # ###### 从微信服务器请求user_info
    openid = wechat_info['openid']
    access_token = wechat_info['access_token']
    expires_in = wechat_info['expires_in']

    user_info = None
    try:
        user_info_url = WECHAT_URL_CONF['user_info_url'] % (access_token, openid)
        user_info = eval(
            http.send_request(WECHAT_URL_CONF['wechat_api_host'], user_info_url, protocol='https')
        )
        if not user_info:
            raise Exception('请求微信user_info结果为空')
    except Exception, e:
        logs.error('请求微信信息异常: user_info')
        logs.exception(e)
        raise e

    # 微信中sex表示性别，0表示未知，1表示男性，2表示女性
    sex = user_info['sex']
    if sex == 1:
        gender = "M"
    elif sex == 2:
        gender = "F"
    else:
        gender = "U"

    nickname = user_info.get('nickname', '')
    user = User.objects.create_param_user(
        ('username', eggs.gen_uuid1()), password=eggs.gen_uuid1(), gender=gender,
        id_label=User.CUSTOMER
    )
    try:
        avatar_url = user_info.get('headimgurl', '').replace('''\\''', '')
        user.save_avatar(http.request_file(avatar_url))  # 请求远端获取图片并保存
        user.save()
    except Exception, e:
        logs.exception(e)
        raise e
    open_acct = OpenAccount(
        user=user, source_site="wechat", openid=openid, access_token=access_token,
        expires_in=int(expires_in)
    )
    open_acct.save()
    customer = Customer(user=user, name=nickname)
    customer.save()

# --------------------------------------------------微信公众号

def bind_phone_pub(request):
    """微信公众号用户绑定手机号页面"""
    if request.method == "GET":
        return http.template_response(request, 'wechat/bind_phone_pub.html', {})

    elif request.method == "POST":  # 执行注册并登录
        phone = request.POST.get('phone', None)
        validate_code = request.POST.get('validate_code', None)
        if not phone or not validate_code:
            return http.failed('miss phone or validate code')

        if not CodeRecord.objects.check_code(phone, validate_code):
            return http.failed('validate code is invalid')

        try:
            openid = request.GET.get('openid', None)
            user = OpenAccount.objects.get(openid=openid).user
        except Exception, e:
            logs.exception(e)
            return http.failed()

        user.phone = phone
        user.save()

    return http.ok()


class WechatHandle(object):
    """
    贯鱼微信公众号接收消息处理类
    """

    @staticmethod
    def deal_msg(content):
        reply_text = u'功能研发中，敬请期待！'
        if content.content == '1':
            WechatHandle.push_msg_to_user(WECHAT_INSTANCE.message.source, 'A1023', '15位')
            reply_text = u'今天天气不错！'

        response = WECHAT_INSTANCE.response_text(content=reply_text)
        return response

    @staticmethod
    def deal_voice(content):
        response = WECHAT_INSTANCE.response_voice(content.media_id)
        return response

    @staticmethod
    def deal_image(content):
        return WECHAT_INSTANCE.response_image(media_id=content.media_id)

    @staticmethod
    def deal_short_video(content):
        response = WECHAT_INSTANCE.response_video(media_id=content.media_id)
        return response

    @staticmethod
    def deal_video(content):
        response = WECHAT_INSTANCE.response_video(media_id=content.media_id)
        return response

    @staticmethod
    def deal_event(content):
        if content.type == "subscribe":  # 关注事件，执行注册（不考虑取消关注后删除用户）
            # 判断当前用户是否已注册
            register_state = u"已经注册"
            try:
                OpenAccount.objects.get(openid=content.source)
            except OpenAccount.DoesNotExist:  # 未注册，执行注册
                try:
                    user_info = WECHAT_INSTANCE.get_user_info(content.source)
                    gender = "U"
                    if user_info['sex'] == 1:
                        gender = "M"
                    elif user_info['sex'] == 2:
                        gender = "F"

                    user = User.objects.create_param_user(('username', eggs.gen_uuid1()),
                                                          nickname=user_info['nickname'],
                                                          acct_type='O', gender=gender)
                    try:
                        user.save_avatar(http.request_file(user_info['headimgurl']))  # 请求远端获取图片并保存
                        user.save()
                    except Exception, e:
                        logs.exception(e)

                    user.cache()
                    open_acct = OpenAccount(user=user, source_site='weixin.qq.com', openid=content.source,
                                            access_token='',
                                            expires_in=None)
                    open_acct.save()
                    register_state = u"执行注册成功"
                except Exception, e:
                    logs.exception(e)
                    register_state = u"执行注册失败"

            except OpenAccount.MultipleObjectsReturned, e:
                logs.exception(e)
                register_state = u"多个相同帐号"

            reply_text = u"感谢您的关注！(%s)" % register_state
            return WECHAT_INSTANCE.response_text(content=reply_text)

        # 获取用户第三方帐号，以便下文使用
        try:
            account = OpenAccount.objects.get(openid=content.source)
        except OpenAccount.DoesNotExist:  # 按理不会出现此类情况，因为在关注的时候就已经注册了
            logs.exception()
            return WECHAT_INSTANCE.response_text(content=u'出错了，请重新关注公众号')
        except OpenAccount.MultipleObjectsReturned, e:
            logs.exception(e)
            return WECHAT_INSTANCE.response_text(content=u'系统繁忙')

        shop_id = WECHAT_SHOP_ID  # 因为是特定公众号，所以shop为特定的某一个 （修改此值）todo
        busi_type = "A"
        shop = Shop.objects.get_cached(shop_id)
        fish_queue = shop.get_user_fish_in_queue(account.user)

        if content.key == "sort_right_now":  # 立即排队按钮
            # 判断用户是否已绑定手机号码
            if not account.user.phone:  # 未绑定手机号
                reply_text = u"到号时，为了方便您及时收到短信或语音通知，需要验证您的手机号。" \
                             u"<a href='%s?openid=%s'>>>点击验证</a>" % (WECHAT_BIND_PHONE_URL, content.source)
                return WECHAT_INSTANCE.response_text(content=reply_text)

            if fish_queue:  # 已经在队列中
                reply_text = u"您已经在队列中，可点击排队查询按钮查询排队进展。"

            else:  # 执行排队
                if not shop:
                    return WECHAT_INSTANCE.response_text(content=u'系统繁忙')

                if busi_type not in shop.get_all_busitypes():
                    return WECHAT_INSTANCE.response_text(content=u'暂不支持%s业务的排队操作' % busi_type)

                fish = shop.new_fish(account.user, busi_type)
                reply_text = u"排队成功，您的编号为：%s" % fish.fish_no

            response = WECHAT_INSTANCE.response_text(content=reply_text)

        if content.key == "sort_detail":  # 查询排队按钮
            if not fish_queue:  # 没有在队列中
                reply_text = u"您还未加入队列，可点击<立即排队>按钮进行"
                return WECHAT_INSTANCE.response_text(content=reply_text)

            person, time = shop.query_fish_status_in_queue(fish_queue)
            if not person:
                reply_text = u"您已到号，请及时前往办理。"
                return WECHAT_INSTANCE.response_text(content=reply_text)

            reply_text = u"您的编号为：%s\n" \
                         u"您前面还有：%s人\n" \
                         u"大约需要等待：%s分钟" % (fish_queue.fish_no, person, time)

            response = WECHAT_INSTANCE.response_text(content=reply_text)

        return response

    @staticmethod
    def push_msg_to_user(openid, keyword1, keyword2):
        """推送消息到微信帐号"""
        if not openid or not keyword1 or not keyword2:
            return False

        data = {
            'first': {
                'value': WECHAT_TEMPLATE['first'],
                'color': 'black'
            },
            'keyword1': {
                'value': keyword1,
                'color': 'purple'
            },
            'keyword2': {
                'value': keyword2,
                'color': 'purple'
            },
            'remark': {
                'value': WECHAT_TEMPLATE['remark'],
                'color': 'black'
            }
        }
        WECHAT_INSTANCE.send_template_message(openid, WECHAT_TEMPLATE['id'], data)
        return True


@csrf_exempt
def wechat(request):
    """贯鱼微信公众号处理视图函数"""
    if request.method == "GET":
        # 检验合法性
        # 从 request 中提取基本信息 (signature, timestamp, nonce, xml)
        signature = request.GET.get('signature')
        timestamp = request.GET.get('timestamp')
        nonce = request.GET.get('nonce')

        if not WECHAT_INSTANCE.check_signature(signature=signature, timestamp=timestamp, nonce=nonce):
            return http.normal_response('check failed')

        return http.normal_response(request.GET.get('echostr', ''), content_type="text/plain")
    else:
        try:
            data = str(request.body).decode('utf-8')
            WECHAT_INSTANCE.parse_data(data=data)
        except ParseError, e:
            logs.exception(e)
            return http.normal_response('Invalid XML Data')

        # 获取解析好的微信请求信息
        message = WECHAT_INSTANCE.get_message()

        response = WECHAT_INSTANCE.response_text(content=DEFAULT_RESPONSE)
        if isinstance(message, TextMessage):
            response = WechatHandle.deal_msg(message)

        elif isinstance(message, VoiceMessage):
            response = WechatHandle.deal_voice(message)

        elif isinstance(message, ImageMessage):
            response = WechatHandle.deal_image(message)

        elif isinstance(message, ShortVideoMessage):
            response = WechatHandle.deal_short_video(message)

        elif isinstance(message, VideoMessage):
            response = WechatHandle.deal_video(message)

        elif isinstance(message, EventMessage):
            response = WechatHandle.deal_event(message)

        return http.normal_response(response, content_type="application/xml")

