# coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
#

"""
LOCAL SETUP:  微信公众帐号基本配置信息
"""

# 微信帐号信息配置
WECHAT_ACCOUNT_CONF = {
    'token':        'line_fish',
    'appid':        'wxba5139c4f13f776b',
    'app_secret':   'a66b59b1a05bdc5eb6d97f7e8606cf7d'
}

# 测试帐号URL
AUTH_REDIRECT_URL = "http://fish.wangkailin.cn/fish/wechat/login"

# 微信url配置
WECHAT_URL_CONF = {
    # 微信授权页面
    'open_authorize': 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s?%s'
                      '&response_type=code&scope=snsapi_userinfo&state=123#wechat_redirect',

    # 获取微信用户openid和用户信息
    'wechat_api_host':  'api.weixin.qq.com',
    'openid_info_url':  '/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code',
    'user_info_url':    '/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN',
}