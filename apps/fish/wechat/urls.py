#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by kylin, on 16-4-18
#

"""

"""
from django.conf.urls import patterns, url

from fish.wechat import views

urlpatterns = patterns(
    '',
    url(r'^$', views.wechat),   # 微信公众号服务

    # -----------------------------以下为微信登录网页业务
    url(r'^index$',         views.index),
    url(r'^login$',         views.wechat_login),
    url(r'^bind_phone$',    views.bind_phone),
    url(r'^bind_phone_pub$',views.bind_phone_pub),  # 公众号的绑定手机

    url(r'^fish_detail$',   views.fish_detail), # 已排队详情
)
