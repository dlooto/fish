# coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
#

"""
微信公众帐号基本配置信息
"""

from wechat_sdk import WechatBasic

import settings

DEFAULT_RESPONSE = u"欢迎光临本店！么么哒！"

# 微信端设置的消息推送模板id
WECHAT_TEMPLATE = {
    'id':       'vqeL3U1FR5dacAFv0FFXqhGnsj3dj-c-ArtnpZ4lclk',
    'first':    u'尊敬的客户您好！',
    'remark':   u'祝生活愉快！么么哒！！！\ue105',
}

# 微信公众号的绑定手机号url。和web的绑定逻辑有差别
WECHAT_BIND_PHONE_URL = "http://linefish.cn/fish/wechat/bind_phone_pub"

# 微信公众号所特对应的门店id
WECHAT_SHOP_ID = 1

# 微信帐号信息配置
# WECHAT_ACCOUNT_CONF = {
#     'token':        'linefish',
#     'appid':        'wxbef623a4e93744ae',
#     'app_secret':   'dc01dea59514dd75ebc7deade5dbcc9b'
# }

WECHAT_ACCOUNT_CONF = {
    'token':        'linefish',
    'appid':        'wxd4a2fb32a5fbcac2',
    'app_secret':   'fc731bc6550ed077fb21bca6d55d39d4'
}

# 贯鱼帐号
AUTH_REDIRECT_URL = "http://linefish.cn/fish/wechat/login"

# 微信url配置
WECHAT_URL_CONF = {
    # 微信授权页面
    'open_authorize': 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s?%s'
                      '&response_type=code&scope=snsapi_userinfo&state=123#wechat_redirect',

    # 获取微信用户openid和用户信息
    'wechat_api_host':  'api.weixin.qq.com',
    'openid_info_url':  '/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code',
    'user_info_url':    '/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN',
}



try:
    if settings.DEBUG:
        from conf_local import *
    else:
        pass
except ImportError, e:
    raise e
