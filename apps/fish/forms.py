#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/18
#

"""

"""

import logging
import re

from core.forms import BaseForm
from fish.models import Organization
from users.models import User, CodeRecord
from utils import eggs

logs = logging.getLogger(__name__)


PASSWORD_COMPILE = re.compile(r'^\w{6,18}$')


class OrgnSignupForm(BaseForm):
    """

    """

    def __init__(self, req, data, *args, **kwargs):
        BaseForm.__init__(self, data, *args, **kwargs)
        self.req = req

    def is_valid(self):
        if not self.check_phone():
            return False
        if not self.check_password():
            return False
        if not self.check_orgn_name():
            return False

        return True

    def save(self):
        phone = self.data.get('phone')
        password = self.data.get('password')
        orgn_name = self.data.get('orgn_name')
        industry = self.data.get('industry')

        admin_user = User.objects.create_param_user(('phone', phone), password=password,
                                                    id_label=User.ORGAN_ADMIN)
        orgn = Organization.objects.create_orgn(
            **{'user': admin_user, 'orgn_name': orgn_name, 'industry': industry}
        )

        return orgn

    def check_phone(self):
        phone = self.data.get('phone')
        if not phone or not eggs.is_phone_valid(phone):
            self.err_msg = u'无效的电话号码'
            return False

        vcode = self.data.get('vcode', '')
        is_valid = CodeRecord.objects.check_code(phone, vcode)
        if not is_valid:
            self.err_msg = u'短信验证码无效'
            return False

        try:
            user = User.objects.get(phone=phone.strip())
            self.err_msg = u'手机号已被注册'
            return False
        except User.DoesNotExist:
            return True

    def check_password(self):
        password = self.data.get('password')
        if not password or not PASSWORD_COMPILE.match(password):
            self.err_msg = u"密码只能为6-16位英文字符或下划线组合"
            return False

        return True

    def check_orgn_name(self):
        orgn_name = self.data.get('orgn_name')
        if not orgn_name:
            self.err_msg = '机构/企业名称不能为空'
            return False
        return True
