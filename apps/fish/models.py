# coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
#

"""
排队核心模型
"""

import sys

from django.db import models, transaction
from django.core.cache import cache

import settings
from core.models import BaseModel
import logging

from fish.queue import ItemQueue
from top.api.base import TopException
from users import sms
from users.models import User, CodeRecord
from users.sms import ali_sms_tpls, ali_conf, ali_call_tpls
from utils import times, eggs
import string

logs = logging.getLogger(__name__)

from fish.managers import ShopManager, OrganizationManager, QueueFishManager, \
    CustomerManager, ShopAuthManager

reload(sys)
sys.setdefaultencoding("utf-8")


class Organization(BaseModel):
    """
    机构数据模型. 机构注册需要进行资质认证
    """

    AUTH_CHECKING = 1  # 等待审核...
    AUTH_APPROVED = 2  # 审核通过
    AUTH_FAILED = 3  # 审核失败.

    # 机构认证审核状态选项
    AUTH_STATUS_CHOICES = (
        (AUTH_CHECKING, u'等待审核'),
        (AUTH_APPROVED, u'审核通过'),
        (AUTH_FAILED, u'认证失败/审核未通过'),  # 如过号等
    )

    user = models.ForeignKey('users.User', verbose_name=u'管理员')

    orgn_name = models.CharField(u'企业/机构名称', max_length=80, )
    industry = models.CharField(u'行业类型', max_length=80, null=True, default='')
    address = models.CharField(u'企业/机构地址', max_length=100, null=True, blank=True, default='')  # 文字位置, 后期可加上地图坐标
    contact = models.CharField(u'联系电话', max_length=20, null=True, blank=True, default='')

    auth_status = models.SmallIntegerField(u'认证状态', choices=AUTH_STATUS_CHOICES,
                                           default=AUTH_CHECKING)

    objects = OrganizationManager()

    class Meta:
        verbose_name = u'1-Organization'
        verbose_name_plural = u'1-Organization'

    def __unicode__(self):
        return u'%s' % self.id

    def is_authed(self):
        """
        若已认证/审核, 返回True
        :return: True of False
        """
        return self.auth_status == self.AUTH_APPROVED

    def is_checking(self):  # 是否等待审核中
        return self.auth_status == self.AUTH_CHECKING

    def output_auth_status(self):
        if self.is_authed():
            return u'审核通过'
        if self.auth_status == self.AUTH_CHECKING:
            return u'审核中'
        if self.auth_status == self.AUTH_FAILED:
            return u'审核未通过'

    def create_shop(self, shop_no, shop_name, user_name, user_phone, addr, contact):
        # user = User.objects.create_param_user(('phone', user_phone), nickname=user_name,
        #                                       is_active=False, acct_type='I',
        #                                       id_label=User.SHOP_ADMIN)
        # new_shop = Shop.objects.create_shop(self, shop_no, shop_name, user=user, address=addr,
        #                                     contact=contact)
        new_shop = Shop.objects.create_shop(self, shop_no, shop_name, user=None, address=addr,
                                            contact=contact)
        new_shop.cache()
        return new_shop

    def create_default_shop(self, user=None, shop_no='s100100', shop_name=u'未命名'):
        new_shop = Shop.objects.create_shop(self, shop_no, shop_name=shop_name, user=user)
        new_shop.cache()
        return new_shop

    def get_all_shops(self):
        return Shop.objects.filter(organization=self)

    def get_all_clerk(self):  # 临时添加该方法
        qs = Clerk.objects.filter(shop__in=self.get_all_shops())
        # print qs.query
        return qs

    def save(self, *args, **kwargs):  # 重写save函数，当后台保存表单后，需要更新缓存
        super(self.__class__, self).save(*args, **kwargs)
        self.cache()

    # def create_clerk(self, shop, clerk_no, password, clerk_name):
    #     """
    #     机构内添加坐席. 本质上是为机构内的某个门店添加坐席, 所以shop为必传参数
    #     """
    #     return shop.create_clerk(clerk_no, password, clerk_name)


class ShopBusiType(BaseModel):  # TODO: 后续完善
    """营业点/商店业务类型记录
    该模型记录由营业点/机构管理员在后台添加, 以便于可机构业务类型动态可配置.
    """

    shop = models.ForeignKey('fish.Shop', verbose_name=u'门店')
    busi_no   = models.CharField(u'业务编号', max_length=20, )
    busi_name = models.CharField(u'业务类型名', max_length=40, )

    is_valid = models.BooleanField(u'是否有效', default=True)

    class Meta:
        verbose_name = u'业务类型记录'
        verbose_name_plural = u'业务类型记录'

    def __unicode__(self):
        return u'%s' % self.id


max_fish_no_str = 'max_fish_no_{0}_{1}'  # 缓存店铺当天某类型业务最大fish_no, 该串用于缓存key前缀


class Shop(BaseModel):
    """
    营业点/商店数据模型. 营业点管理员以手机号或微信进行注册/登录
    """

    # 该属性关联营业点管理员账号身份, 各营业点坐席相关由该管理员进行管理
    user = models.ForeignKey('users.User', verbose_name=u'门店管理员', null=True, blank=True)
    organization = models.ForeignKey('fish.Organization', verbose_name=u'所属机构')

    # 业务编号(非系统分配的id)
    shop_no = models.CharField(u'门店编号', max_length=30, )

    shop_name = models.CharField(u'门店名称', max_length=80, )
    address = models.CharField(u'门店地址', max_length=100, null=True, blank=True, default='')  # 文字位置, 后期可加上地图坐标
    contact = models.CharField(u'联系电话', max_length=20, null=True, blank=True, default='')

    objects = ShopManager()

    class Meta:
        verbose_name = u'2-Shop'
        verbose_name_plural = u'2-Shop'

    def __unicode__(self):
        return u'%s' % self.id

    def __get_queue_key(self, busi_type='A'):
        return u'fish_queue_%s_%s' % (self.id, busi_type)

    def get_fish_queue(self, busi_type='A'):
        """获取当前正在办理的排号队列, 根据类型"""
        if hasattr(self, 'fish_queue') and self.fish_queue:
            return self.fish_queue

        self.fish_queue = cache.get(self.__get_queue_key())
        if not self.fish_queue:
            self.fish_queue = self.init_fish_queue()
            self.cache_fish_queue(self.fish_queue)

        return self.fish_queue

    def cache_fish_queue(self, fish_queue):
        """将坐席当前办理的排号用户, 放入缓存"""
        cache.set(self.__get_queue_key(), fish_queue, timeout=0)

    def init_fish_queue(self):
        """初始化排号队列. 从DB中添加数据到fish_queue
        """
        logs.debug("Start initializing fish_queue...")

        fish_queue = ItemQueue()
        pending_fishes = self.query_pending_fishes()
        for fish in pending_fishes:
            fish_queue.put(fish)
        return fish_queue

    def push_to_queue(self, fish, busi_type):
        """将排号fish加入队列"""
        queue = self.get_fish_queue()
        queue.put(fish)
        self.cache_fish_queue(queue)
        self.fish_queue = queue

    def cache_max_fish_no(self, max_fish_no, busi_type=''):
        """缓存当天生成的某种业务类型最大fish_no, 以用于后续生成新的fish_no"""
        cache.set(max_fish_no_str.format(self.id, busi_type), max_fish_no, timeout=0)

    def get_cached_max_fish_no(self, busi_type='A'):
        """返回当天生成的某业务类型最大fish_no"""
        return cache.get(max_fish_no_str.format(self.id, busi_type))

    def get_from_queue(self):
        fish_queue = self.get_fish_queue()
        return fish_queue.get()

    def remove_from_queue(self, fish):
        """ 坐席呼叫排队用户成功并触发开始办理业务后, 从队列里将排队用户移除.
        更新队列后, 同时更新缓存
        """
        fish_queue = self.get_fish_queue()
        fish_queue.remove(fish)
        self.cache_fish_queue(fish_queue)
        self.fish_queue = fish_queue

    @transaction.commit_manually        # 事务处理
    def create_clerk(self, phone, clerk_no, clerk_name=''):
        """添加坐席"""
        try:
            user = User.objects.create_param_user(('phone', phone), is_active=False,
                                                  id_label=User.SHOP_CLERK) # 置为未激活状态
            success, result = CodeRecord.objects.gen_code(phone, tpl_id=ali_sms_tpls['kcode'],
                                                          len=6, re_gen=True)
            if not success:
                raise Exception('短信验证获取失败')

            fields = {'user': user, 'shop': self, 'clerk_no': clerk_no, 'clerk_name': clerk_name}
            clerk = Clerk(**fields)
            clerk.save()
            transaction.commit()
            return clerk
        except Exception, e:
            logs.exception(e)
            transaction.rollback()
            logs.info('Creating Clerk exception, data rolled back.')
            return None

    def get_all_busitypes(self):
        """返回所有业务类型对象"""
        # return ShopBusiType.objects.filter(shop=self)
        return 'A',  # 暂定只支持一种业务类型

    def get_all_clerks(self):
        return Clerk.objects.filter(shop=self)

    def get_pending_fish_count(self, busi_type=''):
        """获取当前某业务类型的排队人数
        :param busi_type: 为空表示返回全部类型的排队用户数
        """
        return self.get_fish_queue().qsize()

    def query_pending_fishes(self):
        """从持久存储中获取当天所有有效排号.
        返回结果按created_time从小到大进行排序
        """
        today_params = Shop._make_today_params(times.today())
        logs.debug(today_params)
        query_fishes = QueueFish.objects.filter(shop=self, is_valid=True, fish_status=QueueFish.PENDING,
                                                **today_params).order_by('fish_no')
        # logs.debug(query_fishes.query)
        return query_fishes

    @staticmethod
    def _make_today_params(today):
        """ 构造时间查询参数, today为datetime类型 """
        return {'created_time__year': today.year, 'created_time__month': today.month,
                'created_time__day': today.day}

    def generate_fish_no(self, busi_type):
        """
        根据业务类型生成排队号. 仅允许在 Shop.new_fish中被调用
        """

        # if not hasattr(self, 'max_fish_no_dict'):
        #     self.max_fish_no_dict = {}  # 存放shop各业务类型当前最大fish_no号, 以便于生成新的fish_no

        fish_num = self.get_cached_max_fish_no(busi_type)
        if fish_num:
            return self._make_fish_no(busi_type, int(fish_num)+1)

        max_fish = self.get_fish_queue(busi_type).last()
        if not max_fish:
            return self._make_fish_no(busi_type, 1)

        return self._make_fish_no(busi_type, int(max_fish.fish_no[1:]) + 1)

    def _make_fish_no(self, busi_type, fnum):
        self.cache_max_fish_no(fnum, busi_type)
        return "%s%s" % (busi_type, string.rjust(str(fnum), 3, "0"))

    def new_fish(self, customer, busi_type):    # TODO...
        """
        生成一次新的排号记录.
        :param customer: User类型对象
        :param busi_type: 办理业务类型

        :return 新的fish排号对象
        """
        if self.get_user_fish_in_queue(customer): # 若用户已排队, 则不重复生成fish
            logs.warn('Customer is in queue now: %s' % customer.id)
            return

        fields = {'shop': self, 'customer': customer, 'busi_type': busi_type}
        fish = QueueFish(**fields)
        fish.fish_no = self.generate_fish_no(busi_type)
        fish.save()
        self.push_to_queue(fish, busi_type)
        return fish

    def next_fish(self):
        """
        坐席呼叫下一个排号用户.
        """
        return self.get_from_queue()

    def get_user_fish_in_queue(self, customer):
        """查询用户是否已在当前排队队列中, 有则返回该次排号, 否则返回空"""
        for fish in self.get_fish_queue().queue():
            if fish.customer == customer:
                return fish

        return None

    def query_fish_status_in_queue(self, fish):  # TODO...
        """
        获取用户排队状态.
        :param fish: 一次用户排队, 获取该次排队在队列中的状态
        :return: 前面还有几个, 大约需要多少时间
        """
        if not fish:
            return 0, 0

        index = self.get_fish_queue().index(fish)
        person = index + 1
        return person, person * 5

    def get_all_serving_customers(self):
        """
        获取当前所有正在办理业务的Customer
        """
        all_serving_customer = []
        for clerk in self.get_all_clerks():
            serving_fish = clerk.get_current_serving_fish()
            if not serving_fish:
                continue

            customer = serving_fish.customer
            all_serving_customer.append(customer)
        return all_serving_customer

    # ###################################################
    def check_secret_key(self, key):
        """商店接入授权是否有效"""
        try:
            auth = ShopAuth.objects.get(shop=self, key=key)
            return auth.is_authorized()
        except ShopAuth.DoesNotExist:
            return False

    def get_auth_key(self):
        try:
            auth = ShopAuth.objects.get(shop=self)
            return auth.key if auth.is_authorized() else ''
        except ShopAuth.DoesNotExist:
            return ''

    def gen_key(self, ):
        """生成key"""
        return ShopAuth.objects.generate(self, )

    def renew_key(self):
        """刷新key"""
        return ShopAuth.objects.renew(self, )


class Clerk(BaseModel):
    """
    坐席/业务员数据模型.
    添加坐席时隐式注册用户账号, 设置username=<shop_id>_<clerk_no>, 则坐席登录时需要填写:
    shop_id+clerk_no
    """

    user = models.ForeignKey('users.User', verbose_name=u'用户')
    shop = models.ForeignKey('fish.Shop', verbose_name=u'所在门店')

    clerk_no = models.CharField(u'坐席/业务员编号', max_length=10, )
    clerk_name = models.CharField(u'坐席/业务员名称', max_length=40, null=True)

    class Meta:
        verbose_name = u'3-Clerk'
        verbose_name_plural = u'3-Clerk'

    def __unicode__(self):
        return u'%s' % self.id

    def _current_serving_fish_key(self):
        return u'current_serving_fish_%s' % self.user_id

    def get_current_serving_fish(self):
        """得到当前正在办理的排号"""
        return cache.get(self._current_serving_fish_key())

    def set_current_serving_fish(self, fish):
        """将坐席当前办理的排号用户, 放入缓存"""
        cache.set(self._current_serving_fish_key(), fish, timeout=0)

    def send_notice(self, fish):
        """通知排队用户进行业务办理, 以短信/微信消息/电话语音等方式
        """
        customer_phone = fish.customer.phone
        shop_name = u'%s ' % self.shop.shop_name
        sms_params = "{'shop_name': %s, 'fish_no': %s, 'clerk_no': %s}" % \
                     (shop_name.decode('utf-8'), str(fish.fish_no), str(self.clerk_no))
        logs.debug('message_params: %s, send_to_phone: %s' % (sms_params, customer_phone))

        # Maybe start a new thread to send_call ...
        try:
            if settings.VOICE_CALL_ENABLED:
                # sms_params = "{'user_no': %s, 'clerk_no': %s}" % (str(fish.fish_no), str(self.clerk_no))  # TODO: 临时
                # logs.debug('message_params: %s, send_to_phone: %s' % (sms_params, phone))
                sms.aliyun_send_call(customer_phone, sms_params, ali_call_tpls['b_tpl']) # ali_call_tpls['b_tpl']

            if settings.SMS_CALL_ENABLED:
                # sms_params = "{'shop_name': %s, 'fish_no': %s, 'clerk_no': %s}" % (
                #     shop_name.decode('utf-8'), str(fish.fish_no), str(self.clerk_no))
                # logs.debug('message_params: %s, send_to_phone: %s' % (sms_params, phone))
                sms.aliyun_send_sms(customer_phone, sms_params, ali_sms_tpls['bank'])

            return True, 'Send call success'
        except TopException, e:
            logs.exception(e)
            return False, 'Aliyun_send_sms Exception'
        except Exception, e:
            logs.exception(e)
            return False, 'Send notice exception'


class Customer(BaseModel):
    """
    客户数据模型. 客户使用微信open_id与平台进行关联, 或者绑定手机号进行关联
    """
    user = models.ForeignKey('users.User', verbose_name=u'关联用户')
    name = models.CharField(u'昵称', max_length=40, null=True, blank=True)
    phone = models.CharField(u'绑定手机号', max_length=18, null=True, blank=True)    # 用户绑定的手机号

    objects = CustomerManager()

    class Meta:
        verbose_name = u'Customer'
        verbose_name_plural = u'Customer'

    def __unicode__(self):
        return u'%s' % self.id


class QueueFish(BaseModel):
    """
    用户在营业点进行一次排号的数据模型.
    数据在每天营业结束后清空, 并转存到ArchiveQueueFish TODO
    """

    PENDING = 1     # 等待中
    CALLED  = 2     # 已叫号
    PASSED  = 3     # 已过号

    BUSI_STARTED = 8  # 业务办理已开始
    DONE = 9        # 业务已办理完成
    UNFINISHED  = 10 # 业务办理未完成结束

    # 业务类型需要做成管理端可配置 (后续)
    A_BUSI = 'A'
    B_BUSI = 'B'
    C_BUSI = 'C'

    # 排队用户状态选项
    FISH_STATUS_CHOICES = (
        (PENDING,   u'等待中'),
        (CALLED,    u'已叫号'),
        (PASSED,    u'已过号'),

        (BUSI_STARTED,  u'业务办理已开始'),
        (DONE,          u'业务办理已完成'),
        (UNFINISHED,    u'业务办理未完成结束'),  # 如过号等
    )

    # 业务类型
    BUSI_TYPE_CHOICES = (
        (A_BUSI, u'个人业务'),
        (B_BUSI, u'公司业务'),
        (C_BUSI, u'其他业务'),
    )

    shop = models.ForeignKey('fish.Shop', verbose_name=u'门店')
    customer = models.ForeignKey(Customer, verbose_name=u'Customer')
    fish_no = models.CharField(u'排队号', max_length=15, )     # 如 A019

    clerk_no = models.CharField(u'办理坐席编号', max_length=10, null=True, blank=True)  # 如 1015, 用户未进行办理时该属性空
    busi_type = models.CharField(u'办理业务类型', max_length=2, choices=BUSI_TYPE_CHOICES,
                                 null=True, blank=True, default='A')  # 办理业务类型 A-个人业务, B-公司业务
    fish_status = models.SmallIntegerField(u'排队状态', choices=FISH_STATUS_CHOICES,
                                           default=PENDING)

    called_time = models.DateTimeField(u'叫号时间', null=True)         # 叫号时间
    is_valid = models.BooleanField(u'排号是否有效', default=True)

    objects = QueueFishManager()

    class Meta:
        verbose_name = u'4-Fish'
        verbose_name_plural = u'4-Fish'

    def __unicode__(self):
        return u'%s' % self.id

    def set_called(self, clerk_no):
        self.clerk_no = clerk_no
        self.fish_status = self.CALLED
        self.called_time = times.now()
        self.is_valid = False
        self.save()

    def set_passed(self, clerk_no):
        self.clerk_no = clerk_no
        self.fish_status = self.PASSED
        self.is_valid = False
        self.save()


class ArchiveQueueFish(BaseModel):
    """
    营业点排队数据归档模型. 模型结构与QueueFish一致, 用于存储排队历史数据
    """
    shop = models.ForeignKey('fish.Shop', verbose_name=u'门店')
    customer = models.ForeignKey(Customer, verbose_name=u'Customer')
    fish_no = models.CharField(u'排队号', max_length=15, )  # 如 A019
    clerk_no = models.CharField(u'办理坐席编号', max_length=10, null=True, blank=True)  # 如 1015, 用户未进行办理时该属性空
    busi_type = models.CharField(u'办理业务类型', max_length=2, null=True, blank=True, default='A')  # 办理业务类型 A-个人业务, B-公司业务
    fish_status = models.SmallIntegerField(u'排队状态')

    called_time = models.DateTimeField(u'叫号时间', null=True)  # 叫号时间
    is_valid = models.BooleanField(u'排号是否有效', default=True)

    class Meta:
        verbose_name = u'排队归档历史'
        verbose_name_plural = u'排队归档历史'

    def __unicode__(self):
        return self.id


class ShopAuth(BaseModel):
    """门店接入授权数据模型. 每个门店对应唯一记录
    """
    key = models.CharField(max_length=40, primary_key=True, default=eggs.gen_uuid1)
    shop = models.OneToOneField('fish.Shop', verbose_name=u'门店',)
    expires = models.DateTimeField(u'有效期至', null=True, blank=True)      # 表示该时间之前有效
    is_valid = models.BooleanField(u'是否有效', default=False)

    objects = ShopAuthManager()

    class Meta:
        verbose_name = u'ShopAuth'
        verbose_name_plural = u'ShopAuth'

    def __unicode__(self):
        return u'%s' % self.key

    def is_authorized(self):
        """key是否合法并有效的"""
        return self.is_valid and self.expires > times.now()

    def refresh(self):
        self.key = eggs.gen_uuid1()
        self.save()
