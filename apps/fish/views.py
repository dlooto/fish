#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/19
#

"""
该模块内的views直接返回模板response, 或HttpResponse
"""

import logging

from django.http import HttpResponseForbidden
from django.views.generic import TemplateView, View
from django.contrib.auth import logout as system_logout

from core.decorators import login_required
from fish.models import Organization, Clerk, Shop
from utils import http

logs = logging.getLogger(__name__)


class OrgnIndexView(TemplateView):
    template_name = "fish/orgn_index.html"

    def get(self, req, **kwargs):
        if not req.user.is_authenticated():
            return http.redirect_response('/users/w/login')

        context = self.get_context_data(**kwargs)

        organ_obj = req.user.get_owned_orgn()
        if not organ_obj:
            try:
                organ_obj = Organization.objects.get(user=req.user)
                logs.debug(organ_obj)
            except Organization.DoesNotExist:  # TODO: 后续重构, 从login入口进行控制.
                logs.warn('The login account no organization')
                system_logout(req)

                context.update({'err_msg': u'用户名或密码错误'})
                self.template_name = 'users/login.html'
                return self.render_to_response(context)

        clerks = organ_obj.get_all_clerk()
        shops = organ_obj.get_all_shops()
        context.update({'orgn': organ_obj, 'clerks': clerks, 'shops': shops})
        return self.render_to_response(context)


class OrgnSignupView(TemplateView):
    template_name = "fish/orgn_signup.html"

    def get(self, req, **kwargs):
        phone = req.GET.get('phone', '')
        context = self.get_context_data(**kwargs)
        context.update({'phone': phone})
        return self.render_to_response(context)


class ShopIndexView(TemplateView):
    template_name = "fish/shop_index.html"

    def get(self, req, shop_id, **kwargs):
        try:
            if not req.user.is_authenticated():
                return http.redirect_response('/users/w/login')

            shop = Shop.objects.get_cached(shop_id)
            if req.user.get_owned_orgn() != shop.organization and req.user != shop.user:
                return http.redirect_response('/fish/w/unauthed_access')
            elif not req.user.get_owned_orgn().is_authed():
                return http.redirect_response('/fish/w/unauthed_access')

            context = self.get_context_data(**kwargs)

            context.update({'shop': shop, 'clerks': shop.get_all_clerks(),
                            'is_authed': shop.organization.is_authed()})
            return self.render_to_response(context)
        except Exception, e:
            logs.exception(e)
            return http.redirect_response('/fish/w/unauthed_access')


class ShopEditView(TemplateView):
    """门店编辑页面"""
    template_name = "fish/shop_edit.html"

    def get(self, req, shop_id, **kwargs):
        user = req.user
        if not user.is_authenticated():
            return http.redirect_response('/users/w/login')

        shop = Shop.objects.get_cached(shop_id)

        # 用户既不是店铺机构管理员也不是店铺管理员，则没有权限
        if user.get_owned_orgn() != shop.organization and user != shop.user:
            return http.redirect_response('/fish/w/unauthed_access')
        elif not user.get_owned_orgn().is_authed():
            return http.redirect_response('/fish/w/unauthed_access')

        ctx = self.get_context_data(**kwargs)
        ctx.update({'shop': shop})
        return self.render_to_response(ctx)


class ClerkIndexView(TemplateView):
    """坐席主页"""
    template_name = "fish/clerk_index.html"

    def get(self, req, **kwargs):
        if not req.user.is_authenticated():
            return http.redirect_response('/users/w/login')

        ctx = self.get_context_data(**kwargs)
        clerk = req.user.get_clerk()
        if not clerk:
            raise Exception('No clerk related for current user')
        shop = clerk.shop
        cur_server_fish = clerk.get_current_serving_fish()
        cur_serving_fish_no = cur_server_fish.fish_no if cur_server_fish else ''
        ctx.update({'clerk': clerk, 'shop': shop, 'pending_fish_count': shop.get_pending_fish_count(),
                    'cur_serving_fish_no': cur_serving_fish_no,
                    'fishs': shop.get_fish_queue().queue()[:10]})
        return self.render_to_response(ctx)


class OrgnLoginView(View):
    """登录"""

    template_name='fish/orgn_login.html'

    def get(self, req, *args, **kwargs):
        return http.template_response(req, self.template_name, {})

    def post(self, req, *args, **kwargs):
        pass

    # def post(self, req):
    #     form = UserLoginForm(req, data=req.POST)
    #     if form.is_valid():
    #         user = form.login(req)
    #         response = serialize_response(user)
    #
    #         token = user.get_authtoken()
    #         if not token:
    #             return http.resp('authtoken_error', 'Auth error')
    #         response.set_cookie('authtoken', token)
    #         return response
    #
    #     return http.failed(form.err_msg)