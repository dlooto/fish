#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
#

from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView

from fish import api_views, views

urlpatterns = patterns(
    '',

    # API views
    url(r"^orgn_signup$",       api_views.OrgnSignupView.as_view(), ),
    url(r'^create_shop$',       api_views.CreateShopView.as_view(), ),       # 创建门店
    url(r'^edit_shop/(\d+)$',   api_views.EditShopView.as_view(), ),         # 编辑门店
    url(r'^delete_shop/(\d+)$', api_views.DeleteShopView.as_view(), ),       # 删除门店
    url(r'^create_clerk$',      api_views.CreateClerkView.as_view(), ),      # 创建业务员
    url(r'^delete_clerk/(\d+)$', api_views.DeleteClerkView.as_view(), ),     # 删除业务员

    url(r'^new_fish$',           api_views.JoinQueueView.as_view(), ),       # 加入排队
    url(r'^query_queue$',        api_views.QueryQueueView.as_view(), ),      # 查询排队状态
    url(r'^next_fish$',          api_views.CallNextFishView.as_view(), ),    # 呼叫下一个排队用户
    url(r'^pass_fish$',          api_views.PassFishView.as_view(), ),        # 过号
    url(r'^deal_fish_over$',     api_views.DealFishOverView.as_view(), ),    # 完成业务办理

    # ###############################################################################
    # Web pages
    url(r'^about$', TemplateView.as_view(template_name='fish/about/about.html')),
    url(r'^join$',  TemplateView.as_view(template_name='fish/about/join_us.html')),
    url(r'^price$', TemplateView.as_view(template_name='fish/about/price.html')),
    url(r'^help$',  TemplateView.as_view(template_name='fish/about/help.html')),

    url(r'^w/orgn_signup$', views.OrgnSignupView.as_view()),
    url(r'^w/orgn_login$',  TemplateView.as_view(template_name='fish/orgn_login.html')),
    url(r'^w/orgn_index$',  views.OrgnIndexView.as_view(), ),

    url(r'^w/clerk_login$', TemplateView.as_view(template_name='fish/clerk_login.html')),
    url(r'^w/clerk_index$', views.ClerkIndexView.as_view(), ),

    url(r'^w/shop_index/(\d+)$', views.ShopIndexView.as_view(), ),
    url(r'^w/shop_index/(\d+)/edit$', views.ShopEditView.as_view(), ),  # 编辑shop信息
    url(r'^w/unauthed_access$',   TemplateView.as_view(template_name='fish/unauthed_access.html')),


    url(r'^wechat/',        include('fish.wechat.urls')),
)