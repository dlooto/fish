#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
#

"""

"""

import logging

from core.decorators import login_required_pro
from core.views import CustomAPIView
from fish.forms import OrgnSignupForm
from fish.models import Shop, Clerk
from users.forms import is_user_exist
from utils import http, eggs


# 不用从其他模块中引用, 而应每个模块新建一个logger
logs = logging.getLogger(__name__)


# #### Usage:
# try:
#     i = 1
# except: Exception, e:
#     logger.exception(e)  # 自动记录traceback, 并以ERROR等级log
#     logger.info("Hello logger")


class OrgnSignupView(CustomAPIView):
    """
    机构注册, 也即机构管理员注册
    """

    LOGIN_AFTER_SIGNUP = True  # 默认注册后自动登录

    def post(self, req):
        form = OrgnSignupForm(req, data=req.POST)
        if form.is_valid():
            try:  # 此句数据库操作较多，异常可能性较大
                orgn = form.save()  # todo 因涉及三个表的写入，考虑事务回滚操作
            except Exception, e:
                logs.exception(e)
                return http.failed('服务异常')

            admin_user = orgn.user
            if self.LOGIN_AFTER_SIGNUP:
                admin_user.post_login(req)

            response = http.ok({'admin_uid': admin_user.id,
                                'orgn_name': orgn.orgn_name,
                                'industry': orgn.industry})
            token = admin_user.get_authtoken()
            if not token:
                return http.resp('authtoken_error')
            response.set_cookie('authtoken', token)
            return response

        return http.failed(form.err_msg)


class CreateShopView(CustomAPIView):
    """
    创建营业点/门店
    """
    @login_required_pro
    def post(self, req):
        """
        处理基本流程为：
        1.检查当前用户是不是机构管理员
        2.检查所填写的电话号码是否已被注册
        3.创建店铺（将填写的用户名称和电话注册为一个未激活的门店管理员用户，并以此作为店铺的user）
        """
        organ = req.user.get_owned_orgn()
        if not organ:
            return http.failed('您还不是机构管理员')

        shop_no = req.POST.get('shop_no', '').strip()
        shop_name = req.POST.get('shop_name', '').strip()
        user_name = req.POST.get('shop_admin_name', '').strip()
        user_phone = req.POST.get('shop_admin_phone', '').strip()
        addr = req.POST.get('shop_addr', '').strip()
        contact = req.POST.get('shop_contact', '').strip()
        if not shop_no:
            return http.failed('门店编号不能为空')
        if not shop_name:
            return http.failed('门店名称不能为空')
        # 因对应业务逻辑不清，故而先注释掉门店管理员相关代码
        # if not shop_name:
        #     return http.failed('管理员名字不能为空')
        # if not user_phone:
        #     return http.failed('管理员电话号码不能为空')
        #
        # # 检查电话号码是否已注册（设计之初，一个电话号码只能是一个user对象）
        # if eggs.is_phone_valid(user_phone) and is_user_exist({'phone': user_phone}):
        #     return http.failed('该手机号已注册')
        try:
            organ.create_shop(shop_no, shop_name, user_name, user_phone, addr, contact)
        except Exception, e:
            logs.exception(e)
            return http.failed('添加门店异常')

        return http.ok()


class EditShopView(CustomAPIView):
    """
    编辑营业点/门店
    """

    @login_required_pro
    def post(self, req, shop_id):
        shop = Shop.objects.get_cached(shop_id)

        user = req.user
        # 用户既不是企业管理员也不是店铺管理员，则没有权限
        if user.get_owned_orgn() != shop.organization and user != shop.user:
            return http.failed('no permission')
        elif not user.get_owned_orgn().is_authed():  # 验证是否审核通过
            return http.failed('审核未通过')

        shop_no = req.POST.get('shop_no', '').strip()
        shop_name = req.POST.get('shop_name', '').strip()
        shop_address = req.POST.get('shop_address', '').strip()
        shop_contact = req.POST.get('shop_contact', '').strip()
        if not shop_no:
            return http.failed('门店编号不能为空')
        if not shop_name:
            return http.failed('门店名称不能为空')

        shop.shop_no = shop_no
        shop.shop_name = shop_name
        shop.address = shop_address
        shop.contact = shop_contact
        shop.save()
        shop.cache()
        return http.ok({'next_url': '/fish/w/shop_index/%s' % shop.id})


class DeleteShopView(CustomAPIView):
    """
    删除营业点/门店
    """

    @login_required_pro
    def post(self, req, shop_id):
        shop = None
        try:
            shop = Shop.objects.get_cached(shop_id)
        except Exception, e:
            logs.error(e)
            return http.failed('删除操作异常')
        user = req.user
        # 用户不是店铺所属机构的管理员，则没有权限
        if user.get_owned_orgn() != shop.organization:
            return http.failed('no permission')

        # shop.delete()  # todo 直接删除shop以及shop对应的所有Clerk？还是直接设置shop.is_valid=False?

        return http.ok({'next_url': '/fish/w/orgn_index'})


class CreateClerkView(CustomAPIView):
    """
    管理员创建坐席.  目前仅机构或门店管理员有权限创建坐席
    """

    @login_required_pro
    def post(self, req):
        shop_id = req.POST.get('shop_id', '')
        if not shop_id:
            return http.failed('无效的门店')
        shop = Shop.objects.get_cached(shop_id)
        if not shop:
            logs.debug('Shop not found: %s' % shop_id)
            return http.failed('门店不存在')

        phone = req.POST.get('phone', '').strip()
        if not eggs.is_phone_valid(phone):
            return http.failed('手机号无效')

        if is_user_exist({'phone': phone}):
            return http.failed('手机号已被注册')

        clerk_no = req.POST.get('clerk_no')
        clerk_name = req.POST.get('clerk_name')

        user = req.user
        if not user.is_organ_admin() and not user.is_shop_admin():
            logs.info('No permission to create clerk: user_id %s' % user.id)
            return http.failed('无坐席添加权限, 请联系管理员')

        clerk = shop.create_clerk(phone, clerk_no, clerk_name)
        if not clerk:
            return http.failed('添加坐席失败')
        return http.serialize_response(clerk)


class DeleteClerkView(CustomAPIView):
    """
    删除座席
    """

    @login_required_pro
    def post(self, req, clerk_id):
        # clerk = None
        # try:
        #     clerk = Clerk.objects.get_cached(clerk_id)
        # except Exception, e:
        #     logs.error(e)
        #     return http.failed('删除操作异常')
        # user = req.user
        # # 用户既不是Clerk所属店铺所属机构的管理员，也不是所属店铺的管理员，则没有权限
        # if user.get_owned_orgn() != clerk.shop.organization and user != clerk.shop.user:
        #     return http.failed('no permission')
        #
        # clerk.delete()
        #
        # return http.ok({'next_url': '/fish/w/shop_index/%s' % clerk.shop.id})
        pass


class JoinQueueView(CustomAPIView):
    """
    加入排队. 终端消费者(customer)在商家门店排队并加入队列
    """

    @login_required_pro
    def post(self, req):
        # 检查key值
        shop_id = req.GET.get('shop_id', '')
        key = req.GET.get('key', '')
        if not shop_id or not key:
            return http.failed(u'商家微信接入权限错误')
        shop = Shop.objects.get_cached(shop_id)
        if not shop:  # 商户配置的菜单url传递来的shop_id不存在
            return http.failed(u'商家微信接入权限错误')
        # 检查key值是否有效
        if not shop.check_secret_key(key):
            return http.failed(u'商家微信接入权限错误')

        busi_type = req.POST.get('busi_type')

        shop = Shop.objects.get_cached(shop_id)
        if not shop:
            return http.failed('Shop NOT FOUND: %s' % shop_id)

        if busi_type not in shop.get_all_busitypes():
            return http.failed('暂不支持该业务类型: %s' % busi_type)

        try:
            customer = req.user.get_customer()
            if not customer:
                logs.error('非微信授权登录用户 user_id=%s, 无法进行排队操作' % req.user.id)
                return http.failed('非微信授权登录用户, 无法进行排队操作')
            fish = shop.new_fish(customer, busi_type)
            return http.serialize_response(fish)
        except Exception, e:
            logs.exception(e)
            return http.failed('排队操作异常')


class CallNextFishView(CustomAPIView):
    """
    坐席呼叫下一个用户
    """

    @login_required_pro
    def post(self, req):
        clerk = req.user.get_clerk()
        # 点击呼叫下一个时，应当将正在服务的fish置为空
        # 此行也解决最后一个fish到号后也不能排队的问题，
        clerk.set_current_serving_fish(None)
        if not clerk:
            return http.failed(u'非坐席身份, 无操作权限')

        shop = Shop.objects.get_cached(clerk.shop_id)  # 为保持排队队列结构
        fish = shop.next_fish()
        if not fish:
            return http.failed(u'当前没有排队的用户')

        success, msg = clerk.send_notice(fish)
        if success:
            clerk.set_current_serving_fish(fish)
            fish.set_called(clerk.clerk_no)

            shop.remove_from_queue(fish)
            logs.debug('Fish removed from queue: %s' % fish.id)

            cur_serving_fish_no = fish.fish_no if fish else ''
            response = http.serialize_response(fish)
            # 序列化fish_queue
            fishs = []
            for fish in shop.get_fish_queue().queue()[:10]:
                fishs.append({'fish_no': fish.fish_no, 'name': fish.customer.name})
            response.data.update({'pending_fish_count': shop.get_pending_fish_count(),
                                  'cur_serving_fish_no': cur_serving_fish_no,
                                  'fishs': fishs})
            return response
        return http.failed(msg)


class PassFishView(CustomAPIView):
    """坐席发起过号操作"""

    @login_required_pro
    def post(self, req):
        clerk = req.user.get_clerk()
        clerk.set_current_serving_fish(None)
        if not clerk:
            return http.failed(u'非坐席身份, 无操作权限')

        shop = Shop.objects.get_cached(clerk.shop_id)  # 为保持排队队列结构
        fish = shop.next_fish()
        if not fish:
            return http.failed(u'已无排队用户')

        fish.set_passed(clerk.clerk_no)
        shop.remove_from_queue(fish)
        logs.debug(('Fish passed: %s' % fish.id))

        response = http.serialize_response(fish)
        # 序列化fish_queue
        fishs = []
        for fish in shop.get_fish_queue().queue()[:10]:
            fishs.append({'fish_no': fish.fish_no, 'name': fish.customer.name})
        response.data.update({'pending_fish_count': shop.get_pending_fish_count(),
                              'cur_serving_fish_no': clerk.get_current_serving_fish(),
                              'fishs': fishs})
        return response


class DealFishOverView(CustomAPIView):
    """办理完成操作"""

    @login_required_pro
    def post(self, req):
        clerk = req.user.get_clerk()
        fish = clerk.get_current_serving_fish()
        if not fish:
            return http.failed()

        shop = Shop.objects.get_cached(clerk.shop_id)

        fish.set_called(clerk.clerk_no)
        clerk.set_current_serving_fish(None)

        response = http.serialize_response(fish)
        response.data.update({'pending_fish_count': shop.get_pending_fish_count(),
                              'cur_serving_fish_no': clerk.get_current_serving_fish()})
        return response


class QueryQueueView(CustomAPIView):
    """
    查询排队状态
    """
    @login_required_pro
    def post(self, req):
        shop_id = req.COOKIES.get('shop', None)
        shop = Shop.objects.get_cached(shop_id)
        if not shop:
            return http.failed('Shop not found: %s' % shop_id)
        person, time = shop.query_fish_status_in_queue(req.user)

        return http.JResponse({'person': person, 'time': time})





