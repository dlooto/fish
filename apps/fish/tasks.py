#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by kylin, on 16-5-11
#

"""
定时调度任务
"""

import logging
from celery import task
from fish.models import Shop, QueueFish, ArchiveQueueFish

logs = logging.getLogger(__name__)


@task
def clear_cache_and_move_db():  # 每日凌晨2点执行
    """转移数据库内容，并清除所有shop中的所有类型业务的queue"""
    # 转移数据库
    try:
        today_data = QueueFish.objects.all()
        if today_data:
            # 复制数据
            ArchiveQueueFish.objects.bulk_create(today_data)
            # 删除原数据
            QueueFish.objects.all().delete()
        # 清空shop的Queue
        for shop in Shop.objects.all():
            # shop.init_fish_queue()
            for busi_type in shop.get_all_busitypes():  # 定时清空每个商户的每一种业务类型的最大编号
                shop.cache_max_fish_no('%s000' % busi_type, busi_type)

        logs.info('move data from QueueFish to ArchiveQueueFish ok. max fish no clear ok.')
        return True
    except Exception, e:
        logs.error('task clear_cache_and_move_db error')
        logs.exception(e)
        return False

