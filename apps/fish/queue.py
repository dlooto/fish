#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/22
#

"""

"""

import logging

logs = logging.getLogger(__name__)


class ItemQueue():
    """
    仿Python原生Queue.Queue自定义该队列类. 以便于更灵活的队列处理操作
    """

    def __init__(self, maxsize=9999):
        """
        :param maxsize: 队列最大长度
        """
        self.maxsize = maxsize
        self._queue = []

    def queue(self):
        return self._queue

    def empty(self):
        """判断队列是否为空"""
        return not self._queue or len(self._queue) == 0

    def clear(self):
        """清空队列"""
        self._queue = []

    def put(self, item):
        """入队列. 元素将被加到队尾"""
        if self.qsize() >= self.maxsize:
            raise Exception('Out of queue range: %s' % self.maxsize)
        self._queue.append(item)

    def qsize(self):
        """返回队列长度"""
        return len(self._queue)

    def get(self):
        """ 获取队列头部元素, 且不移除该对象 """
        return self._queue[0] if not self.empty() else None

    def last(self):
        """ 获取队尾元素 """
        return self._queue[self.qsize()-1] if not self.empty() else None

    def index(self, item):
        """获取元素在队列中的位置(下标), 以0开始"""
        return self._queue.index(item)

    def dget(self):
        """ 获取队列头部元素并移除该对象 """
        try:
            return self._queue.pop(0)
        except Exception, e:
            logs.exception(e)
            return None

    def remove(self, item):
        """ 移除队列中的指定元素 """
        try:
            self._queue.remove(item)
            return True
        except Exception, e:
            logs.exception(e)
            return False
