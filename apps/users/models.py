#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created on 2016/4/10, by Junn
# 
#

import logging

from django.contrib.auth.backends import ModelBackend
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from rest_framework.authtoken.models import Token

import settings
from sms import CodeRecordManager
from managers import CustomUserManager
from core.models import BaseModel
from django.contrib.auth import login, get_user_model
from utils import images, eggs
from utils import times

logs = logging.getLogger(__name__)


ACCT_TYPE_CHOICES = (
    ('E', u'显式注册'),   #正常流程注册
    ('I', u'邀请注册'),   #被邀请形式隐式注册
    ('O', u'第3方登录注册') 
)


VALID_ATTRS = ('phone', 'nickname', 'gender', 'birth', 'email')


class MyModelBackend(ModelBackend):
    """
    重写authenticate方法, 使得可以使用username/phone/email任一账号类型登录
    """

    def authenticate(self, password=None, **kwargs):
        UserModel = get_user_model()
        auth_key = kwargs.get('auth_key')
        if auth_key not in UserModel.VALID_AUTH_FIELDS:
            return None
        try:
            user = UserModel._default_manager.get(**{auth_key: kwargs.get(auth_key)})
            if user.check_password(password):
                return user
        except UserModel.DoesNotExist:
            return None


class User(AbstractBaseUser, PermissionsMixin, BaseModel):
    """
    用户数据基础模型.
    可用username/phone/email其中之一进行注册. 注册后另外2个属性可继续绑定并作为登录账号(需加登录标识).
    """

    NORMAL_USER = 'NU'   # 一般用户, 默认注册类型
    CUSTOMER = 'CU'      # 消费者customer, 参与排队的用户
    SHOP_CLERK = 'SC'    # 门店坐席
    SHOP_ADMIN = 'SA'    # 门店管理员
    ORGAN_ADMIN = 'OA'   # 企业管理员

    ID_LABELS_CHOICES = (
        (NORMAL_USER, u'一般用户'),
        (CUSTOMER, u'消费者'),
        (SHOP_CLERK, u'坐席'),
        (SHOP_ADMIN, u'门店管理员'),
        (ORGAN_ADMIN, u'企业管理员'),
    )

    MALE = 'M'
    FEMALE = 'F'
    UNKNOWN = 'U'

    GENDER_CHOICES = (
        (MALE, u'Male'),
        (FEMALE, u'Female'),
        (UNKNOWN, u'Unknown'),
    )

    username = models.CharField(u'用户名', max_length=255, unique=True, blank=True, null=True, default='')
    phone = models.CharField(u'手机号', max_length=18, blank=True, null=True, default='')
    email = models.EmailField('Email', blank=True, null=True, default='')

    is_staff = models.BooleanField(_('staff status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)
    date_joined = models.DateTimeField(u'注册时间', auto_now_add=True)
    acct_type = models.CharField(u'账号类型', max_length=1, choices=ACCT_TYPE_CHOICES, default='E')  # 账号注册类型
    id_label = models.CharField(u'身份标识', max_length=2, choices=ID_LABELS_CHOICES, default='NU')  # 账号注册类型

    # 该字段仅存储文件名(不包括路径), 大图小图同名且以不同的路径区分
    avatar = models.CharField(u'头像', max_length=80, blank=True, null=True, default='')
    nickname = models.CharField(u'昵称', max_length=32, null=True, blank=True, default='')
    birth = models.DateField(u'生日', null=True, blank=True, auto_now_add=True)
    gender = models.CharField(u'性别', max_length=1, choices=GENDER_CHOICES, default='U')
    
    login_count = models.IntegerField(u'登录次数', default=0)
    last_login_ip = models.GenericIPAddressField(u'最后登录IP', null=True, blank=True)

    USERNAME_FIELD = 'username'
    VALID_AUTH_FIELDS = ['username', 'phone', 'email']  # 允许的可用于注册/登录的有效属性字段
    backend = 'users.models.MyModelBackend'

    objects = CustomUserManager()

    def __unicode__(self):
        return self.nickname if self.nickname else self.username

    class Meta:
        verbose_name = u'用户'
        verbose_name_plural = u'用户'
        app_label = 'users'
        swappable = 'AUTH_USER_MODEL'
        
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    def info(self):
        """输出用于log显示"""
        return u'%s, %s, %s, %s' % (self.id, self.username, self.phone, self.email)

    def update(self, data, new_avatar=None):
        return self

    def save_avatar(self, avatar_file):
        """保存头像文件到FS, 同时生成指定尺寸的小图"""

        self.avatar = images.save_image(
            avatar_file, eggs.gen_uuid1() + '.jpg',
            thumb_size=(90, 90), create_thumb=True, cate='avatar'
        )
        
    def get_avatar_path(self):  # 返回头像全路径
        if not self.avatar:
            return ''
        return '%s%s/%s' % (settings.MEDIA_URL, settings.USER_AVATAR_DIR['thumb'], self.avatar)             
                
    def post_login(self, req):
        """
        登录及后续其他处理.
        :param req: django request请求对象
        """
         
        login(req, self)

        if 'HTTP_X_FORWARDED_FOR' in req.META.keys():
            self.last_login_ip = req.META['HTTP_X_FORWARDED_FOR']
        else:
            self.last_login_ip = req.META['REMOTE_ADDR']
        
        self.incr_login_count()    # 登录次数+1
        self.save()  
        self.cache()

    def get_authtoken(self):
        """返回登录鉴权token"""

        try:
            token, created = Token.objects.get_or_create(user=self)
            return token.key if token else ''
        except Exception, e:
            logs.exception(e)
            return ''
            
    def get_short_name(self):
        return self.nickname if self.nickname else self.username

    def get_username(self):
        return self.username

    def get_full_name(self):
        return self.username

    # # 获取用户所属机构或店铺或Clerk对应的名称
    # def get_owned_name(self):
    #     if self.is_organ_admin():
    #         return "%s" % self.get_owned_orgn().orgn_name
    #     elif self.is_shop_admin():
    #         return "%s" % self.get_owned_shop().shop_name
    #     elif self.is_clerk():
    #         return "%s" % self.get_clerk().clerk_name
    #     else:
    #         return "%s" % self.get_customer().name

    # 获取user自己的主页url
    def get_index_url(self):
        if self.is_organ_admin():
            return "/fish/w/orgn_index"
        elif self.is_shop_admin():
            return "/fish/w/shop_index"
        elif self.is_clerk():
            return "/fish/w/clerk_index"
        else:
            return "/"

    def show_gender(self):
        if self.gender == User.MALE:
            return u'男'
        if self.gender == User.FEMALE:
            return u'女'
        return u'未知'

    def incr_login_count(self):
        """登录次数加1"""
        self.login_count += 1

    def is_organ_admin(self):   # 是否企业管理员
        return self.id_label == User.ORGAN_ADMIN

    def is_shop_admin(self):    # 是否门店管理员
        return self.id_label == User.SHOP_ADMIN

    def is_customer(self):      # 是否消费者
        return self.id_label == User.CUSTOMER

    def is_clerk(self):         # 是否坐席
        return self.id_label == User.SHOP_CLERK

    def get_owned_orgn(self):
        """获取该账号所创建的机构. 若返回不为空, 则也说明该账号为机构管理员"""

        if hasattr(self, '_cached_organ') and self._cached_organ:
            return self._cached_organ

        try:
            organization_set = self.organization_set.all()
            if not organization_set:
                return None
            self._cached_organ = organization_set[0]
            return self._cached_organ
        except Exception, e:
            logs.exception(e)
            return None

    def get_owned_shop(self):
        """获取该账号所创建的门店/商店. 若返回不为空, 则说明该user账号为门店管理员"""
        try:
            shop_set = self.shop_set.all()
            if not shop_set:
                return None
            return shop_set[0]
        except Exception, e:
            logs.exception(e)
            return None

    def get_clerk(self):
        """获取该账号所关联的clerk(坐席). 若返回不为空, 则说明该user账号为坐席账号"""
        if hasattr(self, '_cached_clerk') and self._cached_clerk:
            return self._cached_clerk

        try:
            clerk_set = self.clerk_set.all()
            if not clerk_set:
                return None
            self._cached_clerk = clerk_set[0]
            return self._cached_clerk
        except Exception, e:
            logs.exception(e)
            return None

    def get_customer(self):
        """获取该账号所关联的customer对象. 若返回不为空, 则说明该user账号为customer账号"""
        try:
            return self.customer_set.all()[0]
        except Exception, e:
            logs.exception(e)
            return None


class PasswordResetRecord(BaseModel):
    user = models.ForeignKey(User, verbose_name=u'用户')
    key = models.CharField(u'重置密码验证码', max_length=100)
    is_valid = models.BooleanField(u'是否可用', default=True)

    def __unicode__(self):
        return "%s, %s, %s" % (self.user, self.key, self.is_valid)

    class Meta:
        verbose_name = u'重置密码的验证码'
        verbose_name_plural = u'重置密码的验证码'


class MobileBindingRecord(BaseModel):
    user = models.ForeignKey(User, verbose_name=u'用户')
    mobile = models.CharField(u'电话号码', max_length=16)
    bound = models.BooleanField(u'是否绑定', default=True)

    def __unicode__(self):
        return "%s, %s" % (self.user, self.mobile)

    class Meta:
        verbose_name = u'手机绑定记录'
        verbose_name_plural = u'手机绑定记录'


class OpenAccount(BaseModel):
    """ 第3方登录账号 """

    user = models.ForeignKey(User)                                           # 关联user
    source_site = models.CharField(max_length=20)                            # 第三方站点名称
    openid = models.CharField(max_length=80)                                 # 第三方账号的uid
    access_token = models.CharField(max_length=150, blank=True, null=True)   # access_token
    expires_in = models.IntegerField(blank=True, null=True)                  # 过期时间

    def __unicode__(self):
        return u'%s, %s' % (self.user, self.source_site)

    class Meta:
        verbose_name = u'第3方登录账号'
        verbose_name_plural = u'第3方登录账号'


class CodeRecord(BaseModel):
    """短信验证码记录"""

    phone = models.CharField(u'电话号码', max_length=16)
    code = models.CharField(u'验证码', max_length=16)
    is_valid = models.BooleanField(u'是否可用', default=True)

    objects = CodeRecordManager()

    def __unicode__(self):
        return "%s, %s" % (self.phone, self.code)

    class Meta:
        verbose_name = u'短信验证码'
        verbose_name_plural = u'短信验证码'

    def check(self):
        pass

    def is_expired(self):
        """是否已过期"""
        delta = timezone.now() - self.created_time
        return delta.seconds > settings.SMS_CODE_EXPIRY * 60
