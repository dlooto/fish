#coding=utf8

from django.conf import settings


def profile(req):
    """载入用户信息"""

    user = req.user
    if user.is_authenticated():
        if user.is_organ_admin():
            profile_name = user.get_owned_orgn().orgn_name
        elif user.is_clerk():
            profile_name = user.get_clerk().clerk_no
        elif user.is_shop_admin():
            profile_name = user.get_owned_shop().shop_name
        else:
            profile_name = u'%s' % user.phone or user.email or user.username
    else:
        profile_name = user.username

    return {
        'profile_name': profile_name
    }

