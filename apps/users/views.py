#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/19
#

"""
Web Views. 直接响应template或HttpResponse
"""

import logging

from django.views.generic import TemplateView
from django.contrib.auth import logout as system_logout

from utils import http

logs = logging.getLogger(__name__)


class LoginView(TemplateView):
    template_name = "users/login.html"

    def get(self, req, **kwargs):
        if not req.user.is_authenticated():
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)

        user = req.user
        user_index_url = ''
        if user.is_organ_admin():
            user_index_url = '/fish/w/orgn_index'
        elif user.is_clerk():
            user_index_url = '/fish/w/clerk_index'
        else:
            user_index_url = '/fish/w/shop_index'  # TODO: Un implemented

        return http.redirect_response(user_index_url)


class LogoutView(TemplateView):
    template_name = "index.html"

    def get(self, req, **kwargs):
        if req.user.is_authenticated():
            system_logout(req)
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)

        logs.warn('not logged in user do logout...')
        return http.redirect_response('/')


class UserInfoView(TemplateView):
    template_name = "users/user_info.html"

    def get(self, req, **kwargs):
        if req.user.is_authenticated():
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)

        return http.redirect_response('/users/w/login')


class SettingView(TemplateView):
    template_name = "users/setting.html"

    def get(self, req, **kwargs):
        if req.user.is_authenticated():
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)

        return http.redirect_response('/users/w/login')


class ChangePWDView(TemplateView):
    template_name = "users/change_pwd.html"

    def get(self, req, **kwargs):
        if req.user.is_authenticated():
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)

        return http.redirect_response('/users/w/login')


class ResetPWDView(TemplateView):
    template_name = "users/reset_pwd.html"

    def get(self, req, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

        return http.redirect_response('/users/w/login')


# class ActivateUserView(TemplateView):
#     template_name = ""