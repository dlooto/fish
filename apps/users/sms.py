#coding=utf-8
#
# Copyright (C) 2016  NianNian TECH Co., Ltd. All rights reserved.
# Created on 2016-4-12, by Junn
#

"""
短信服务模块. 或许可以考虑提取出来放到单独的包里...
"""

import logging
import urllib
import httplib
import json
import sys

from core.models import BaseManager
import settings
from django.utils import timezone
from top.api.base import TopException
from utils import eggs

import top.api

logs = logging.getLogger(__name__)

reload(sys)
sys.setdefaultencoding('utf-8')

# 阿里大鱼短信接口配置
ali_conf = {
    'appkey':   "23346077",
    'secret':   'be8642ec28d460f5b32347d33f8cb79c',
    'domain':   'gw.api.taobao.com',                # 正式环境域名
    'port':     80,
    # 'domain':      'gw.api.tbsandbox.com',  # 沙箱环境域名
    'resp_key':  'alibaba_aliqin_fc_sms_num_send_response',  # 短信发送接口

    'sign_name':  u"贯鱼排队",
    'sign_name2': u"贯鱼",
}

# 阿里短信模板
ali_sms_tpls = {
    'vcode': "SMS_7765286",  # 默认为短信验证码模板
    'kcode': 'SMS_8945547',  # 添加坐席时的短信激活码, 6位  模板: 验证码: ${code}

    'bank': 'SMS_7751185',  # 银行 #${shop_name}提醒您, 请${fish_no}号用户到${clerk_no}号窗口办理业务'
    'hosp': 'SMS_7730991',  # 医院 ${shop_name}提醒您, 请${fish_no}号用户到${clerk_no}号桌取餐或就餐
    'cater':'SMS_7786369'   # 餐饮 ${shop_name}提醒您, 请${fish_no}号用户到${clerk_no}号桌取餐或就餐
}

# 语音通知TTS模板
ali_call_tpls = {
    'normal': 'TTS_7756112',   # 请${user_no}号用户到${clerk_no}号窗口办理业务

    'b_tpl':  'TTS_8580036',   # 银行 ${shop_name}提醒您, 请${fish_no}号用户到${clerk_no}号窗口办理业务
    'h_tpl':  'TTS_8615142',   # 医院 ${shop_name}提醒您, 请${fish_no}号到${clerk_no}号诊室就诊
    'c_tpl':  'TTS_8660098',   # 餐饮 ${shop_name}提醒您, 请${fish_no}号用户到${clerk_no}号桌取餐或就餐
}


def aliyun_send_sms(phone, sms_params, tpl_id, sign_name=ali_conf['sign_name2']):
    """
    阿里短信发送接口

    :param phone:      短信要送达的手机号
    :param sms_params: 短信内容参数, 字典类型, 形如 {'code': 1234}, 具体参数设置由所用短信模板而定
    :param tpl_id:     短信模板id
    :param sign_name:  短信签名
    :return:   正常发送返回True, 否则返回False及错误消息

    """
    req = top.api.AlibabaAliqinFcSmsNumSendRequest(domain=ali_conf['domain'], port=ali_conf['port'])
    req.set_app_info(top.appinfo(ali_conf['appkey'], ali_conf['secret']))

    req.extend = "123456"
    req.sms_type = "normal"

    req.sms_free_sign_name = sign_name
    req.sms_template_code = tpl_id
    req.rec_num = phone
    req.sms_param = sms_params

    try:
        resp = req.getResponse()
        logs.debug(resp)
        result = resp.get(ali_conf['resp_key'], {}).get('result')
        if result.get('success'):
            return True, result.get('msg')
        return False, result.get('msg')
    except TopException, e:
        logs.exception('短信接口请求异常(aliyun_send_sms): \n %s' % e)
        return False, u'短信接口异常'
    except Exception, e:
        logs.exception(e)
        return False, u'验证码短信发送异常'


# 语音通知呼叫时的显示号码
show_phones = {
    # 400号
    '3782': '4001003782',
    '3817': '4001003817',
    '5027': '4001005027',
    '7032': '4001007032',
    '8052': '4001008052',

    # 固定电话
    '0514_3270': '051482043270',
    '0514_3272': '051482043272',
    '0514_3274': '051482043274',
}


def aliyun_send_call(phone, tts_params, tpl_id=''):
    """ 拨打语音电话 """
    req=top.api.AlibabaAliqinFcTtsNumSinglecallRequest(domain=ali_conf['domain'], port=ali_conf['port'])
    req.set_app_info(top.appinfo(ali_conf['appkey'], ali_conf['secret']))

    req.extend = "12345"
    req.tts_param = tts_params
    req.called_num = phone
    req.called_show_num = show_phones['0514_3274']  # show_phones['7032']
    req.tts_code = tpl_id
    try:
        resp= req.getResponse()
        print(resp)
    except Exception,e:
        print(e)

# 云片验证码短信模板编号
SMS_TPL = {
    'signup': 900055,  # 短信服务后台上的短信模板id
    'bind':   900037,
    'reset':  900073,

    'simple': 1323775,
}

# 云片短信服务设置
YUNPIAN_SMS = {
    'apikey': 'ba4dd04659c7546fa01806e5e9638c78',
    'host':   'sms.yunpian.com',
    'port':   443,
    'sms_send_uri':  "/v2/sms/single_send.json",        # 与sms_tpl_send_uri两者选其一

    'sms_tpl_send_uri': "/v2/sms/tpl_single_send.json"  # 模板短信接口的URI
}


def send_sms(mobile, text):
    """ 通用接口发短信. 返回是否成功, 及对应的短信接口调用结果信息.

    @return: 发送成功返回True, 否则返回False
    """

    params = urllib.urlencode({'apikey': YUNPIAN_SMS.get('apikey'), 'text': text, 'mobile':mobile})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = httplib.HTTPConnection(YUNPIAN_SMS.get('host'), port=YUNPIAN_SMS.get('port'), timeout=30)
    conn.request("POST", YUNPIAN_SMS.get('sms_send_uri'), params, headers)
    response = conn.getresponse()
    response_str = response.read()
    conn.close()

    result = json.loads(response_str)
    if result.get('code') == 0: # 短信发送成功
        return True, result
    return False, result


def tpl_send_sms(mobile, tpl_id, tpl_value):
    """
    使用模板接口发短信
    """
    params = urllib.urlencode({'apikey': YUNPIAN_SMS.get('apikey'), 'tpl_id': tpl_id,
                               'tpl_value': urllib.urlencode(tpl_value), 'mobile': mobile})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = httplib.HTTPSConnection(YUNPIAN_SMS.get('host'), port=YUNPIAN_SMS.get('port'), timeout=30)
    conn.request("POST", YUNPIAN_SMS.get('sms_tpl_send_uri'), params, headers)
    response = conn.getresponse()
    response_str = response.read()
    conn.close()

    # print type(response_str)
    # print response_str

    try:
        result = json.loads(response_str)
        if result.get('code') == 0:         # 短信发送成功
            return True, result
        return False, result
    except Exception, e:
        logs.exception(e)
        return False, 'Exception occurred when'


# 同一手机号当天可请求的短信验证码最大条数
code_max_limit = settings.SMS_CONF['code_max_limit']


class CodeRecordManager(BaseManager):

    def check_code(self, phone, vcode, expiry=settings.SMS_CONF['code_expiry']):
        """
        检验短信验证码.
        :return: 若验证码有效, 返回True, 否则返回False
        """

        coderecord = self.get_valid_code(phone, vcode, expiry)
        if not coderecord:
            logs.info(u'无效的code: phone %s, code %s' % (phone, vcode))
            return False

        logs.debug('%s' % coderecord)
        coderecord.update(is_valid=False)   # 验证成功后, 置为无效
        return True

    def gen_code(self, phone, tpl_id=ali_sms_tpls['vcode'], re_gen=False,
                 len=settings.SMS_CONF['code_len'], ):
        """
        生成短信验证码并发送到手机号. 默认验证码长度4位

        :param phone: 同一个手机号同一操作(注册/绑定/重置密码其中一种操作)一天之内最多能获取
                    MAX_SMS_LIMIT次验证码.若该手机号码有未使用过的验证码(is_valid=True),
                    则不重新生成新的验证码
        :param re_gen: 当re_gen为True时, 将使该号码之前获取的所有验证码失效, 重新生成.
                       该参数设定可用控制短信发送数目

        :return
        """

        today = timezone.now().date()
        k = self.filter(phone=phone)

        # 若当天该手机号已请求了MAX_SMS_LIMIT次验证码, 则直接返回并警告
        if k.filter(created_time__year=today.year, created_time__month=today.month,
                    created_time__day=today.day, is_valid=False).count() >= code_max_limit:
            logs.warn(u'该手机号(%s)一天之内已获取%s次验证码' % (phone, code_max_limit))
            return False, -1  # 请求验证码次数超过限制

        if re_gen:  # 若要重新生成验证码, 则将之前生成的验证码置为无效状态
            k.filter(is_valid=True).update(is_valid=False)

        # 若存在未过期且有效可用的验证码, 则直接使用而不重新生成. 否则生成新的验证码. 有效期10分钟
        k = k.filter(created_time__gt=timezone.now() - timezone.timedelta(
            minutes=settings.SMS_CONF['code_expiry']), is_valid=True)
        if k and k[0]:
            code = k[0].code
        else:
            code = eggs.random_num(len)
            self.create(phone=phone, code=code)

        if not settings.SMS_CODE_REQUIRED:
            return True, code                   # Just for testing ...

        success, result = aliyun_send_sms(phone, {'code': code}, tpl_id)
        if success:
            return success, code

        return False, 0     # 验证码短信发送失败

        # Following for yunpian sms...
        # try:
        #     #success, result = tpl_send_sms(phone, SMS_TPL.get('simple'), {'#code#': code})  # 模板方式发送短信
        #     if success:
        #         return success, code
        #     else:
        #         logs.error('send_sms error: \n %s' % result['msg'].encode('utf8'))
        #         return success, result
        # except Exception, e:
        #     logs.exception('Exception occurred when send_sms: \n %s' % e)
        #     return False, 0

    def get_valid_code(self, phone, vcode, expiry):
        """

        :param phone:  手机号
        :param vcode:  短信验证码
        :param expiry: 超时时间, 单位分钟
        :return:
        """
        return self.filter(phone=phone, code=vcode, is_valid=True,
                           created_time__gt=timezone.now() - timezone.timedelta(
                               minutes=expiry)  # 有效且未超时
                           )