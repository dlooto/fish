#coding=utf-8
#
# Copyright (C) 2016 LineFish. All rights reserved.
# Created on 2016-4-13, by Junn
# 
#
from django.contrib.auth import logout as system_logout

import settings, logging
from users.forms import UserSignupForm, UserLoginForm, is_user_exist
from users.models import CodeRecord, User
from utils.http import Response
from core.views import CustomAPIView
from core import codes
from core.decorators import login_required_pro, debug_allowed
from rest_framework.decorators import api_view
from utils import http, eggs

logs = logging.getLogger(__name__)

PHONE_VCODE_KEY = "vcode_key_{0}" # 手机号验证确认码key


class SignupView(CustomAPIView):

    LOGIN_AFTER_SIGNUP = True  # 默认注册后自动登录

    def post(self, req):
        form = UserSignupForm(req, data=req.POST)
        if form.is_valid():
            user = form.save()

            if not self.LOGIN_AFTER_SIGNUP:
                return http.ok()

            user.post_login(req)
            # 注册并登录后, 仅返回uid
            response = http.ok({'uid': user.id})
            token = user.get_authtoken()
            if not token:
                return http.resp('authtoken_error')
            response.set_cookie('authtoken', token)
            return response

        return http.failed(form.err_msg)


class LoginView(CustomAPIView):
    """
    通过 phone/username/email等登录系统
    """

    def post(self, req):
        form = UserLoginForm(req, data=req.POST)
        if form.is_valid():
            user = form.login(req)

            response = http.serialize_response(user)

            token = user.get_authtoken()
            if not token:
                return http.resp('authtoken_error', 'Auth error')
            response.set_cookie('authtoken', token)

            # 设置登录成功后的跳转页面
            if user.is_organ_admin():
                response.data.update({'next_url': '/fish/w/orgn_index'})
            elif user.is_clerk():
                response.data.update({'next_url': '/fish/w/clerk_index'})
            else:
                response.data.update({'next_url': '/fish/w/shop_index'}) # TODO: Un implemented
            return response

        if form.err_user_not_actived:  # 账号未激活
            logs.info('账号未激活: ')
            return http.resp('passwd_set_required', form.err_msg)

        return http.failed(form.err_msg)


class ActivateAccountView(CustomAPIView):
    """
    账号激活
    """

    def post(self, req):
        phone = req.POST.get('phone', '').strip()
        kcode = req.POST.get('kcode', '').strip()
        password = req.POST.get('password', '').strip()

        if not eggs.is_phone_valid(phone):
            return http.failed('手机号码无效')
        if not eggs.is_password_valid(password):
            return http.failed('密码只能为6-18位英文字符或下划线组合')

        user = None
        try:
            user = User.objects.get(phone=phone)
        except User.DoesNotExist:
            return http.failed('手机号未注册')
        except Exception, e:
            logs.exception(e)
            return http.failed('操作异常')

        if user.is_active:
            return http.failed('账号已激活, 无需重复激活')

        is_valid = CodeRecord.objects.check_code(phone, kcode, 30)
        if not is_valid:
            return http.failed('激活码无效')

        try:
            user.set_password(password)
            user.is_active = True
            user.save()
            return http.ok({'activate_success': True, 'next_url': '/users/w/activate_success'})
        except Exception, e:
            logs.exception(e)
            return http.failed('操作异常')


class ChangePWDView(CustomAPIView):
    """
    修改密码
    """

    def post(self, req):
        if not req.user.is_authenticated():
            return http.failed('登录失效，请重新登录')

        old_password = req.POST.get('old_password', '').strip()
        new_password = req.POST.get('new_password', '').strip()
        new_password_again = req.POST.get('new_password_again', '').strip()

        user = req.user

        if new_password != new_password_again:
            return http.failed('两次密码输入不同')
        if not eggs.is_password_valid(new_password):
            return http.failed('密码只能为6-18位英文字符或下划线组合')
        if not user.check_password(old_password):
            return http.failed('原始密码错误')

        try:
            user.set_password(new_password)
            user.save()
            system_logout(req)
            return http.ok({'next_url': '/users/w/login'})
        except Exception, e:
            logs.exception(e)
            return http.failed('操作异常')


class ResetPWDView(CustomAPIView):
    """
    重置密码
    """

    def post(self, req):
        phone = req.POST.get('phone', '').strip()
        vcode = req.POST.get('vcode', '').strip()
        new_password = req.POST.get('new_password', '').strip()
        new_password_again = req.POST.get('new_password_again', '').strip()

        if not CodeRecord.objects.check_code(phone, vcode):
            return http.failed('验证码无效')
        if new_password != new_password_again:
            return http.failed('两次密码输入不同')
        if not eggs.is_password_valid(new_password):
            return http.failed('密码只能为6-18位英文字符或下划线组合')

        try:
            user = User.objects.get(phone=phone)
            user.set_password(new_password)
            user.save()
            user.cache()
            return http.ok({'next_url': '/users/w/login'})
        except Exception, e:
            logs.exception(e)
            return http.failed('操作异常')


class OpenLoginView(CustomAPIView):

    def post(self, req):
        """  """
        pass


class CheckPhoneView(CustomAPIView):
    """
    验证手机号合法性, 并发送验证码(文字短信或语音短信)
    """

    def post(self, req):
        """
        验证手机号合法性: 格式是否正确, 是否已注册, 若格式正确且未注册则发送短信验证码
        """

        # 临时代码, 防止非法短信请求
        scode = req.REQUEST.get('scode')
        if not scode == settings.SECRET_KEY:
            logs.info('非法短信请求')
            return http.resp('permission_denied')

        # 检查手机号格式
        phone = req.REQUEST.get('phone', '').strip()
        if not eggs.is_phone_valid(phone):
            return http.failed('手机号无效')

        success, result = CodeRecord.objects.gen_code(phone, re_gen=True)
        if not success and result == -1:
            return http.failed('验证码获取次数超过当天限制')

        if success:
            return http.ok({'vcode': result})

        return http.failed('验证码获取异常')


class CheckSmsCodeView(CustomAPIView):
    """
    同一手机号尝试验证码连续5次错误, 则封禁.
    """

    def post(self, req):
        """
        检查短信验证码是否合法, 所有验证码的有效期均为10分钟
        """

        phone = req.POST.get('phone')
        vcode = req.POST.get('vcode')
        if not phone or not vcode:
            return http.failed('参数缺乏: phone/vcode')

        sms_err_count = req.session.get('%s_sms_err_count' % phone, 0)
        # if sms_err_count > settings.SMS_CONF['code_err_limit']:  # 超过一定次数设置验证黑名单, 存入DB
        #   return http.failed('验证码错误次数达最大限制')

        is_valid = CodeRecord.objects.check_code(phone, vcode)
        if not is_valid:
            req.session['%s_sms_err_count' % phone] = sms_err_count + 1
            return http.failed('无效的短信验证码')

        vcode_key = eggs.gen_uuid1()
        req.session.pop('%s_sms_err_count' % phone)
        req.session['vcode_key_%s' % phone] = vcode_key  # TODO 后续处理结果将该值清空
        return http.ok({'vcode_key': vcode_key})  # 通过该key确认是已验证过的手机号. 暂未使用...


class LogoutView(CustomAPIView):

    def post(self, req):
        if req.user.is_authenticated():
            system_logout(req)
            return http.ok()
        return http.failed('un logged in user do logout')


class UsersAction(CustomAPIView):
    
    @debug_allowed
    def get(self, req):
        '''所有用户'''
        return http.serialize_response(list(User.objects.all()))


class UserView(CustomAPIView):
    """
    包含接口:
       更新某用户信息
       获取某用户信息
    """
    
    def get(self, req, uid):
        """get specified user info """
        
        try:
            user = User.objects.get_cached(uid)
            return http.serialize_response(user)
        except User.DoesNotExist:
            return Response(codes.fmat('object_not_found', 'user %s' % uid))
    
    @login_required_pro
    def put(self, req, uid):
        """update an user info"""
        
        user = req.user
        if int(uid) != user.id:
            return http.resp('permission_denied')
        
        user.update(req.DATA, new_avatar=req.FILES.get('avatar', None))
        return http.serialize_response(user, app_name='users')  # cast to concrete class for serializing, or exception occurred  !!!









