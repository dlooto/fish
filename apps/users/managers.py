# coding=utf-8
#
# Created on Mar 21, 2014, by Junn
# 
#

from django.contrib.auth.models import BaseUserManager
from django.utils import timezone

from core.models import BaseManager
from utils import http, eggs
import logging

logs = logging.getLogger(__name__)


VALID_ATTRS = ('nickname', 'email', 'phone', 'gender', 'avatar')

AUTH_TYPE = {
    'P': 'phone',
    'U': 'username',
    'E': 'email',
}


class CustomUserManager(BaseUserManager, BaseManager):
    """
    定制的UserManager
    """

    def _create_user(self, username, password=None, is_active=True, commit=True, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(username=username, is_staff=False, is_active=is_active,
                          is_superuser=False, last_login=now, date_joined=now,
                          **extra_fields)

        user.set_password(password)
        if commit:
            user.save(using=self._db)
        return user

    def get_by_phone(self, phone):
        try:
            return self.get(phone=phone)
        except self.model.DoesNotExist:
            return None

    def update_user(self, user, req):
        data = req.DATA
        for attr in VALID_ATTRS:  # 双重循环, 以后需要改进算法
            if attr in data:
                setattr(user, attr, data.get(attr))

        user.save(using=self._db)
        return user

    def create_param_user(self, auth_obj, password=None, is_active=True, commit=True,
                          **extra_fields):
        """
        可通过username/phone/email其中之一创建新用户账号.

        :param auth_obj: 元组类型, 如('username', 'hardy_zhang'), 或('phone', '15982022344')
                         由此确定用户账号注册类型
        :param password: 密码
        """
        now = timezone.now()
        user = self.model(is_staff=False, is_active=is_active, is_superuser=False,
                          last_login=now, date_joined=now, **extra_fields)
        setattr(user, auth_obj[0], auth_obj[1])
        if not auth_obj[0] == 'username':
            # 因设置了USERNAME_FIELD='username'且 username添加了属性unique=True, 所以用
            # phone或email注册时也需将username设值
            user.username = eggs.gen_uuid1()

        user.set_password(password)
        if commit:
            user.save(using=self._db)
        return user

    def create_superuser(self, username, password, **extra_fields):
        u = self._create_user(username, password, **extra_fields)
        u.is_staff = True
        u.is_superuser = True
        u.save(using=self._db)
        return u

    def create_open_user(self, phone, source_site, openid, access_token, expires_in, open_name='', avatar_url='',
                         gender='U'):
        """创建第3方登录账号
        @param phone:           第3方平台绑定的手机号码（一般情况下，必须有此参数）
        @param source_site:     第3方平台名称
        @param openid:          用户在第3方平台的账号id
        @param access_token:    第3方平台的访问token
        @param expires_in:      access_token的超时时间  
        @param open_name:       用户在第3方平台的昵称 
        @param avatar_url:      用户在第3方平台的头像url
        @param gender:          用户在第3方平台的性别
        """

        from users.models import OpenAccount
        try:
            # 第3方平台注册用户不允许直接登录, 除非重置了密码(重置密码需要先绑定手机号)
            user = self.create_param_user(('phone', phone), nickname=open_name, acct_type='O', gender=gender)
            try:
                user.save_avatar(http.request_file(avatar_url))  # 请求远端获取图片并保存
                user.save()
            except Exception, e:
                logs.error(e)

            user.cache()
            open_acct = OpenAccount(user=user, source_site=source_site, openid=openid, access_token=access_token,
                                    expires_in=int(expires_in))
            open_acct.save()
            return True, user
        except Exception, e:
            logs.error(e)
            return False, u'账号绑定异常'
