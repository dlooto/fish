#coding=utf-8
#
# Created on 2014-9-9, by Junn
#

from core.serializers import BaseModelSerializer, CustomPaginationSerializer
from users.models import User
from rest_framework import serializers
import settings


class UserSerializer(BaseModelSerializer):
    avatar = serializers.SerializerMethodField('get_avatar')

    class Meta:
        model = User
        fields = ('id', 'nickname', 'username', 'email', 'phone', 'id_label',
                  'gender', 'avatar', 'birth', 'acct_type', 'login_count'
        )
        
    def get_birth(self, obj):
        return obj.birth.strftime('%Y-%m-%d')
        
    def get_avatar(self, obj):
        return '%s%s/%s' % (settings.MEDIA_URL,settings.USER_AVATAR_DIR['thumb'], obj.avatar) if obj.avatar else ''  
    

class PagingUserSerializer(CustomPaginationSerializer):

    class Meta:
        object_serializer_class = UserSerializer
        

