#coding=utf-8
#
# Copyright (C) 2015  Niannian Co., Ltd. All rights reserved.
# Created on Mar 21, 2014, by Junn
# 
#
import logging
import re

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import AuthenticationForm, ReadOnlyPasswordHashField
from django.contrib.auth import authenticate

from models import User
from core.forms import BaseForm
from utils import eggs

logs = logging.getLogger(__name__)

PASSWORD_COMPILE = re.compile(r'^\w{6,18}$')
MOBILE_PHONE_COMPILE = re.compile(r'^1[3458]\d{9}$')


class UserSignupForm(BaseForm):
    """用户注册信息验证
    自定义form 规范:
        1. 带is_valid()方法, 对数据参数进行验证
        2. 必要时带save方法

    """

    def __init__(self, req, data, *args, **kwargs):
        BaseForm.__init__(self, data, *args, **kwargs)
        self.req = req

    def is_valid(self):
        auth_key = self.data.get('auth_key')  # 取值为 'phone', 'username', or 'email'
        if auth_key not in ('phone', 'username', 'email'):
            self.err_msg = u'未知的auth_key类型: %s' % auth_key
            return False
        if not self.data.get(auth_key):
            self.err_msg = u'缺少参数值: %s' % auth_key
            return False
        if not getattr(self, 'check_%s' % auth_key)():  # 无效则返回
            return False
        if not self.check_password():
            return False

        return True

    def save(self):
        auth_key = self.data.get('auth_key')
        password = self.data.get('password')
        return User.objects.create_param_user((auth_key, self.data.get(auth_key)),
                                              password=password, is_active=True)

    def check_username(self):
        username = self.data.get('username')
        if not username:
            self.err_msg = u'无效的用户名'
            return False
        return self._check_auth_name('username')

    def check_phone(self):
        phone = self.data.get('phone')
        if not phone or not eggs.is_phone_valid(phone):
            self.err_msg = u'无效的电话号码'
            return False

        # 如果是手机号注册, 需确保手机号已验证
        phone = phone.strip()
        vcode_key = self.data.get('vcode_key') # 注册时传入
        if not vcode_key or vcode_key != self.req.session.get('vcode_key_%s' % phone, ''):
            self.err_msg = u'手机号未验证'
            return False

        return self._check_auth_name('phone')

    def check_email(self):
        email = self.data.get('email')
        if not email or not eggs.is_email_valid(email):
            self.err_msg = u'无效的email'
            return False
        return self._check_auth_name('email')

    def check_password(self):
        password = self.data.get('password')
        if not password or not PASSWORD_COMPILE.match(password):
            self.err_msg = u"密码只能为6-18位英文字符或下划线组合"
            return False

        return True

    def _check_auth_name(self, auth_key):
        if is_user_exist({auth_key: self.data.get(auth_key)}):
            self.err_msg = u'%s已被注册' % auth_key
            return False
        return True


def is_user_exist(auth_obj):
    """
    判断phone/email/username是否已注册
    :param auth_obj: 参数形如  {'phone': 15910012003} 或 {'email': 'xj123@qq.com'}
    :return: 已注册则返回True, 否则返回False
    """
    if not isinstance(auth_obj, dict) or auth_obj.keys()[0] not in User.VALID_AUTH_FIELDS:
        logs.debug('auth_obj 参数格式错误')
        return False

    user = None
    try:
        user = User.objects.get(**auth_obj)  # phone=phone or  username=username or email=email ...
        return True
    except User.DoesNotExist:
        return False


class UserLoginForm(BaseForm):
    """
    验证用户登录数据.
    """

    def __init__(self, req, data=None, *args, **kwargs):
        BaseForm.__init__(self, data, *args, **kwargs)
        self.req = req
        self.user_cache = None

        self.err_user_not_actived = False

    def is_valid(self):
        auth_key = self.data.get('auth_key', '').strip()  # 取值为 'phone', 'username', or 'email'
        if auth_key not in ('phone', 'username', 'email'):
            self.err_msg = u'未知的auth_key类型: %s' % auth_key
            return False

        auth_value = self.data.get(auth_key)
        if not auth_value:
            self.err_msg = u'缺少参数值: %s' % auth_key
            return False

        password = self.data.get('password')
        if not password:
            self.err_msg = u'密码无效'
            return False

        try:
            if auth_key == 'username': # username时需要精确匹配, 区分大小写
                # user = User.objects.get(username__exact=auth_value)  # DB层面大小写无法区分, 所以换用以下语句...
                query_set = User.objects.extra(where=["binary username = '%s'" % auth_value])
                if not query_set:
                    logs.debug('username: %s' % auth_value)
                    logs.debug(query_set.query)
                    self.err_msg = '用户不存在'
                    return False
                user = query_set[0]
            else:
                user = User.objects.get(**{auth_key: auth_value})

            if not user.is_active:
                self.err_msg = '账号未激活'
                self.err_user_not_actived = True
                return False
            if not user.has_usable_password():
                self.err_msg = '未设置密码'
                return False
        except User.DoesNotExist:
            self.err_msg = '用户不存在'
            return False
        except Exception, e:
            logs.exception(e)
            self.err_msg = 'Query user exception'
            return False

        # logs.debug({'auth_key': auth_key, auth_key: auth_value, 'password': password})
        try:
            self.user_cache = authenticate(**{'auth_key': auth_key, auth_key: auth_value, u'password': password, }) # 传入auth_key以标识验证登录类型
        except Exception, e:
            logs.exception(e)
            self.err_msg = '账号验证错误'
            return False

        if not self.user_cache:
            self.err_msg = '账号名或密码无效'
            return False

        # 如果是被邀请注册用户首次登录, 或者无有效密码(第3方账号登录用户首次用手机号登录)
        if not self.user_cache.is_active:
            self.err_msg = '账号未激活'
            return False

        return True

    def login(self, req):
        user = self.user_cache
        user.post_login(req)
        return user


class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    username = forms.RegexField(label=_("Username"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text=_("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput, required=False)   # required=False, 为定制错误消息

    class Meta:
        model = User
        # fields = ("username",)

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        user = None
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if not PASSWORD_COMPILE.match(password):
            self.error_status = 2
            raise forms.ValidationError(u"密码只能为6-18位英文字符或下划线组合。")
        return password

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    username = forms.RegexField(
        label=_("Username"), max_length=30, regex=r"^[\w.@+-]+$",
        help_text=_("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password = ReadOnlyPasswordHashField(label=_("Password"),
        help_text=_("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = User

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class AdminPasswordChangeForm(forms.Form):
    """
    A form used to change the password of a user in the admin interface.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password (again)"),
                                widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(AdminPasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        if not PASSWORD_COMPILE.match(password1):
            raise forms.ValidationError(u"密码只能为6-16位英文字符或下划线组合。")

        return password1

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        """
        Saves the new password.
        """
        self.user.set_password(self.cleaned_data["password1"])
        if commit:
            self.user.save()
        return self.user


class PasswordChangeForm(forms.Form):
    """
    
    """
    password = forms.CharField(widget=forms.PasswordInput)
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password (again)"),
                                widget=forms.PasswordInput, required=False)

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if not self.user.check_password(password):
            raise forms.ValidationError(u'密码输入不正确。')
        return password

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        if not PASSWORD_COMPILE.match(password1):
            raise forms.ValidationError(u"密码只能为6-16位英文字符或下划线组合。")
        if password1 == self.cleaned_data.get('password'):
            raise forms.ValidationError(u"新旧密码不能相同。")
        return password1

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(u"新密码和重复密码不一致。")
        return password2
    
    def set_password(self):
        self.user.set_password(self.cleaned_data.get('password1'))
        self.user.save()


class PasswordResetForm(forms.Form):
    """
    for user reset the password
    """
    password1 = forms.CharField(label=_(u'Password'), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password (again)"), widget=forms.PasswordInput,
                                required=False)

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordResetForm, self).__init__(*args, **kwargs)

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        if not PASSWORD_COMPILE.match(password1):
            raise forms.ValidationError(u"密码只能为6-16位英文字符或下划线组合。")
        return password1

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(self.error_messages['password_mismatch'])
        return password2

    def reset(self):
        user = self.user
        user.set_password(self.cleaned_data['password1'])
        
        if user.is_invited_signup_passwd_set_required():  #若是被邀请注册用户重置密码, 则将账号激活
            user.is_active = True
            
        user.save()


class UserInfoUpdateForm(forms.ModelForm):

    gender = forms.CharField(max_length=1, required=False)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(UserInfoUpdateForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = ('nickname', 'phone', 'gender', 'avatar')

    def update(self, user):
        user.nickname = self.cleaned_data.get('nickname') or user.nickname
        user.phone = self.cleaned_data.get('phone') or user.phone
        user.gender = self.cleaned_data.get('gender') or user.gender
        
        avatar = self.cleaned_data.get('avatar')
        if avatar:
            #nickname
            pass
        user.save()
