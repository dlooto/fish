#coding=utf-8
#
# Copyright (C) 2015  Niannian Co., Ltd. All rights reserved.
# Created on Mar 21, 2014, by Junn
# 
#
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from users import api_views, views

import settings

urlpatterns = patterns(
    '',

    url(r'^check_phone$',     api_views.CheckPhoneView.as_view(), ),     # post
    url(r'^check_sms_code$', api_views.CheckSmsCodeView.as_view(), ),   # post

    url(r'^signup$',         api_views.SignupView.as_view(), ),         # post
    url(r'^login$',          api_views.LoginView.as_view(), ),          # post
    url(r'^open_login$',     api_views.OpenLoginView.as_view(), ),      # post
    url(r'^logout$',         api_views.LogoutView.as_view(), ),         # post
    url(r'^activate$',       api_views.ActivateAccountView.as_view(), ),  # post
    url(r'^change_pwd$',     api_views.ChangePWDView.as_view(), ),  # post
    url(r'^reset_pwd$',      api_views.ResetPWDView.as_view(), ),  # post

    # ###########################################################################
    # Web views. 所有与api接口同名的web views函数加前缀 "page_"
    url(r'^w/login$',          views.LoginView.as_view(), ),
    url(r'^w/logout$',         views.LogoutView.as_view(), ),         # post
    url(r'^w/user_info$',      views.UserInfoView.as_view(), ),      # get
    url(r'^w/setting$',        views.SettingView.as_view(), ),       # get
    url(r'^w/change_pwd$',     views.ChangePWDView.as_view(), ),     # get
    url(r'^w/reset_pwd$',      views.ResetPWDView.as_view(), ),  # get

    url(r'^w/activate$',       TemplateView.as_view(template_name='users/activate_account.html')),
    url(r'^w/activate_success$', TemplateView.as_view(template_name='users/activate_success.html')),
)
