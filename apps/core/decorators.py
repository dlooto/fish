#coding=utf-8
import settings
from utils import http

__author__ = 'Junn'

from functools import wraps
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.core.handlers.wsgi import WSGIRequest

from utils.http import JResponse
from core import codes


def login_required(response_type='redirect', redirect_to_view=None, sign_in_redirect_msg=None):
    def _decorator(func):
        @wraps(func)
        def _wrapped_func(request, *args, **kwargs):
            if not request.user or not request.user.is_authenticated():
                if request.is_ajax():
                    context = {}
                    context.update(codes.get('login_required'))
                    return JResponse(context)
                return redirect('%s?next=%s' % (reverse('login'), request.get_full_path()))
            return func(request, *args, **kwargs)
        return _wrapped_func
    return _decorator


def debug_allowed(func):
    def _debug_allowed(obj, req, *args, **kwargs):
        if settings.DEBUG:
            return func(obj, req, *args, **kwargs)
        return http.resp('invalid_request_method')
    return _debug_allowed


def login_required_pro(func):
    """作为class-based view请求函数的装饰器"""
    def _login_required(obj, request, *args, **kwargs):
        if request.user.is_authenticated():
            return func(obj, request, *args, **kwargs)
        return JResponse(codes.get('login_required'))
    return _login_required


def login_required_mtd(func):
    """
    作为method-based view请求函数的装饰器. 可以找方法与login_required_pro合并
    """
    def _login_required(request, *args, **kwargs):
        if request.user.is_authenticated():
            return func(request, *args, **kwargs)
        return JResponse(codes.get('login_required'))
    return _login_required



