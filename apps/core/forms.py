#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/15
#

"""

"""

import logging

logs = logging.getLogger(__name__)


class BaseForm():
    """
    Form表单验证基础类. 各子类对该基础类进行重写  :TODO: 待完善...

    自定义form 规范:
        1. 带is_valid()方法, 对数据参数进行验证
        2. 必要时带save方法

    """

    def __init__(self, data, *args, **kwargs):
        self.err_msg = ''
        self.data = data

    def is_valid(self):
        pass

    def save(self):
        pass