#coding=utf-8
#
# Copyright (C) 2015  Niannian Co., Ltd. All rights reserved.
# Created on Mar 21, 2014, by Junn
# 
#

import logging

from django.core.cache import cache
from django.db import models
from django.db.models import Manager
from utils import eggs

logs = logging.getLogger('django')


class BaseManager(Manager):

    def __init__(self):
        super(BaseManager, self).__init__()

    # ########################################################### cache methods
    def make_key(self, obj_id):
        """现有key生成规则, 需要确保整个工程中所有模型不能同名!!! """
        return u'%s%s' % (self.model.__name__, obj_id)

    def cache_all(self):
        pass
        # """数据量大以后, 需要考虑将数据批量缓存"""
        # objs = self.all()
        # for obj in objs:
        #     obj.cache()
        #
        # logs.info('All %s objects cached.' % self.model.__name__)

    def get_cached(self, obj_id):
        """通过对象id, 获取单个的缓存数据对象"""

        obj = cache.get(self.make_key(obj_id))
        if not obj:
            logs.debug('Null cached obj, get obj from DB ...')
            try:
                obj = self.get(id=int(obj_id))
                obj.cache()
            except self.model.DoesNotExist:
                logs.error('Object not found: %s %s' % (self.model.__name__, obj_id))
                return None
            except Exception, e:
                logs.exception('get_cached object error: \n %s' % e)
                return None

        return obj

    def get_cached_many(self, **kwargs):
        pass


class BaseModel(models.Model):
    created_time = models.DateTimeField(u'创建时间', auto_now_add=True)

    objects = BaseManager()

    class Meta:
        abstract = True
        ordering = ['-created_time']

    def cache(self): # the object cache itself
        cache.set(type(self).objects.make_key(self.id), self, timeout=0)    #永不过期
                
    def clear_cache(self): # clear from cache
        cache.delete(type(self).objects.make_key(self.id))  