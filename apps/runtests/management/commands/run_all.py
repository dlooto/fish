
#coding=utf-8
#
# Copyright (C) 2014  NianNian TECH Co., Ltd. All rights reserved.
# Created on Oct 27, 2015, by Junn
#

"""
运行Tests
"""

import logging

from runtests import tests
from django.core.management.base import BaseCommand

logs = logging.getLogger('fish')


class Command(BaseCommand):
    
    help = "Run Tests"

    def handle(self, *args, **kwargs):
        if len(args) == 0:
            print('The Testing command is needed')
            return
        try:
            getattr(tests, args[0])(*args, **kwargs)
        except Exception, e:
            logs.exception(e)


