#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/19
#

"""

"""

import logging

from utils import http

logs = logging.getLogger(__name__)


def get_user_info(req):  # just for testing
    user = req.user
    if user and user.is_authenticated():
        return http.ok({
            'user_id':  user.id,
            'name':     user.nickname if user.nickname else user.username,
            'phone':    user.phone,
            'email':    user.email
        })
    return http.normal_response('Not logined: %s' % req.user)