#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by junn, on 16/4/19
#

"""

"""

import logging

from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView

from runtests import views

logs = logging.getLogger(__name__)


urlpatterns = patterns('',
    url(r'^next_fish$', TemplateView.as_view(template_name='tests/index.html')),
    url(r'^user$',      views.get_user_info),
    url(r'^page$',      TemplateView.as_view(template_name='fish/clerk_index.html')),

    # test sb-admin
    url(r'^django-sb-admin/',   include('django_sb_admin.urls')),

    url(r'^/blue$', TemplateView.as_view(template_name='tests/bluelight.html')),
)
