#coding=utf-8
#
# Copyright (C) 2015  Niannian Co., Ltd. All rights reserved.
# Created on 2014-4-24, by junn

#

############################################################
##         日志信息配置 
############################################################


import os

# 日志级别:  CRITICAL, ERROR, WARN, INFO, DEBUG

LOG_ROOT = os.path.join(os.path.dirname(__file__), '../../logs')
LOG_FILE_PATH = os.path.join(LOG_ROOT, "fish.log")

FISH_LOG_LEVEL = 'DEBUG'      # 决定写入自定义日志文件的日志级别, 大于等于配置级别的日志都会写入文件
SQL_LOG_LEVEL = 'INFO'        # 是否输出SQL语句, django框架的SQL语句仅在DEBUG级才能输出. 默认仅输出到console
DJANGO_LOG_LEVEL = 'DEBUG'    # 该设置决定django框架自身的日志输出

LOG_SQL_IN_FILE = False       # 是否将debug级别的SQL语句输出写入日志文件

# logger:
#     当给一条消息给logger 时，会将消息的日志级别与logger 的日志级别进行比较。如果消息的日志级别大于等于
# logger 的日志级别，该消息将会往下继续处理。如果小于，该消息将被忽略。
# Logger 一旦决定消息需要处理，它将传递该消息给一个Handler。
#
# handler:
#     也有一个日志级别。如果消息的日志级别小于handler 的级别，handler 将忽略该消息。
#
# Filter:  用于对从logger 传递给handler 的日志记录进行额外的控制。

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(pathname)s:%(lineno)s] %(message)s",
            'datefmt' : "%Y/%m/%d %H:%M:%S"
        },
    },
    'filters': {
         'require_debug_false': {
             '()': 'django.utils.log.RequireDebugFalse'
         }
     },
    'handlers': {
        'file': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': LOG_FILE_PATH,
            'maxBytes': 5*1024*1024,
            'backupCount': 20,
            'formatter': 'standard',
        },
        'mail_admins': {  
            'level': 'ERROR',  
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',  
            'include_html': True,  
        },  
        'console':{
            'level': 'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },

    # django, django.request, django.db.backends为django自带的3个记录器
    'loggers': {
        'django': {
            'handlers':['console','file'], # 同时写到console和file里
            'level': DJANGO_LOG_LEVEL,
            'propagate': True,
        },
        'django.request': { # 所有高于（包括）error的消息会被发往mail_admins处理器，消息不向父层次发送
            'handlers': ['mail_admins', ],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['console', 'file'] if LOG_SQL_IN_FILE else ['console'],
            'level':    SQL_LOG_LEVEL,
            'propagate': False,
        },

        'fish': {   # 所有高于(包括)info的消息同时会被发往console和file处理器
            'handlers': ['console', 'file'],
            'level': FISH_LOG_LEVEL,
        },
        'users': {   # 所有高于(包括)info的消息同时会被发往console和file处理器
            'handlers': ['console', 'file'],
            'level': FISH_LOG_LEVEL,
        },
    }
}


