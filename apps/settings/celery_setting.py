#coding=utf-8
#
# Copyright (C) 2016 LineFish. All Rights Reserved.
# Created by kylin, on 16-5-11
#

"""
celery配置文件
"""


import logging
import djcelery
from celery.schedules import crontab


logs = logging.getLogger(__name__)


djcelery.setup_loader()
BROKER_URL = 'redis://127.0.0.1:6379/1'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'

CELERYBEAT_SCHEDULE = {
    "clear_cache_and_move_db": {
        "task": "fish.tasks.clear_cache_and_move_db",
        "schedule": crontab(hour=2, minute=0),  # Run at 02：00
     },
}

CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'