#coding=utf-8
#
# Copyright (C) 2012-2013 FEIGR TECH Co., Ltd. All rights reserved.
# Created on 2014-4-6, by Junn
#
#

############################################################
##         生产环境参数配置 
############################################################

import os

from base import *

ALLOWED_HOSTS = [
    'localhost',
    '127.0.0.1',

    'www.linefish.cn',
    'linefish.cn',
    'open.linefish.cn',
    'admin.linefish.cn',
]

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'read_default_file': '/etc/mysql/my.cnf',
#         },
#     }
# }

DATABASES = {
    "default": {
        "ENGINE":   'django.db.backends.mysql',
        "NAME":     'fish',
        "USER":     'root',
        "PASSWORD": '_DFn92ke2@nf0',
        "HOST":     'localhost',
        "PORT":     '3306',
    }
}


MIDDLEWARE_CLASSES += [
    #'core.middleware.PrintSqlMiddleware',
    #'core.middleware.PrintRequestParamsMiddleware',
    
]

MEDIA_URL = 'http://linefish.cn/media/'
STATIC_URL = 'http://linefish.cn/static/'

INSTALLED_APPS += ['gunicorn',]

ADMINS = (

)

MANAGERS = ADMINS

# 到号呼叫开关, 默认打开
VOICE_CALL_ENABLED = True   # 是否语音呼叫提醒
SMS_CALL_ENABLED = True     # 是否短信通知提醒



