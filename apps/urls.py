#coding=utf-8

from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.conf.urls.static import static
import settings
from django.views.generic.base import TemplateView

admin.autodiscover()

# handler500 = "django.views.defaults.server_error"
handler500 = "core.views.server_error"

urlpatterns = patterns('',
    url(r"^$",              TemplateView.as_view(template_name='index.html')),
    url(r'^admin/',         include(admin.site.urls)),

    # ### API urls
    url(r'^api/v1/users/',      include('users.urls')),
    url(r'^api/v1/fish/',       include('fish.urls')),

    # ### Web Pages
    url(r'^fish/',  include('fish.urls')),
    url(r'^users/', include('users.urls')),
)

if settings.DEBUG:
    # Used in debug mode for handling user-uploaded files
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # used for testing html page
    urlpatterns += patterns('',
        url(r'^tests/', include('runtests.urls')),
    )


