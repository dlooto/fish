#coding=utf-8
#
# Created on Apr 23, 2014, by Junn
# 
#

import json
import urllib
import httplib
import requests

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.core.files.uploadedfile import SimpleUploadedFile

from rest_framework.response import Response as RfResponse

from core import codes
from utils.eggs import make_instance


def request_file(url):
    """
    从远端下载文件, 并构建request.FILES中的uploaded file对象返回.

    :param url:  文件url路径, 如http://abc.im/12345.jpg

    :return:
        SimpleUploadedFile object, it is containned by the request.FILES(dictionary-like object)
    """
    if not url:
        return
    response = requests.get(url)
    return SimpleUploadedFile('file', response.content)    


def send_request(host, send_url, protocol='http', method='GET', port=80, params={}, timeout=30,
                 headers={"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}):
    """
    发起http或https请求, 并返回结果

    :param:
        send_url = '/api/v2/app/version/541a7131f?token=dF0zeqBMXAP'
        method = 'GET'
        params = {'token': 'dF0zeqAPWs'}
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        host = 'fir.im'
        port = 80

    :return:
        返回响应字符串

    """
    
    encoded_params = urllib.urlencode(params)
    if protocol not in ("http", "https"):
        return None

    if protocol == "http":
        conn = httplib.HTTPConnection(host, port=port, timeout=timeout)
    else:
        port = 443  # https协议支持443端口
        conn = httplib.HTTPSConnection(host, port=port, timeout=timeout)

    conn.request(method, send_url, encoded_params, headers)
    response = conn.getresponse()
    response_str = response.read()
    conn.close()
    
    return response_str


# ##################################################################
# 注: 以下必须声明为函数, 不可声明为常量. 因常量值将只在模块import时被赋值
# #################################################################


def ok(data={}):
    """
    操作成功时返回
    :param data: 为字典类型数据
    :return: json数据
    """
    return JResponse(codes.append('ok', data)) if data else resp('ok')


def failed(msg=''):
    """
    操作失败时返回.
    :param msg: 错误消息
    :return: json数据
    """
    return resp('failed', msg)


def object_not_found():
    return resp('object_not_found')


def http404():
    return resp('not_found')


def resp(crr, msg=''):
    """
    返回常量错误结果,
    :param msg: 可格式化具有占位符的字符串
    :param crr: 错误码标识
    """
    return JResponse(codes.fmat(crr, msg))


def standard_response(template, req, context):
    """返回http Web response"""
    return render_to_response(template, RequestContext(req, context))


def template_response(req, template, ctx):
    """返回模板响应
    """
    return render_to_response(template, RequestContext(req, ctx))


def normal_response(content='', *args, **kwargs):
    """返回普通字符串响应"""
    return HttpResponse(content, *args, **kwargs)


def redirect_response(url):
    """重定向到指定url"""
    return HttpResponseRedirect(url)


def serialize_response(items, app_name=None):
    """
    通用转换方法: 将数据对象列表转换成相应的serializer response对象返回

    @param items:    数据对象列表. 若为QuerySet结果集, 需要先转换成list对象再转入
    @param app_name: 为防止module_name被拼装成 django.serializers, 传入该参数以确保为自定义apps

    @return: Response object

    """
    if not items:
        return Response(items)

    instance = items[0] if isinstance(items, list) else items

    if app_name:
        module_name = app_name + '.serializers'  # like users.serializers
    else:
        module_name = instance.__module__.split('.')[0] + '.serializers'

    class_name = instance.__class__.__name__ + 'Serializer'
    return Response(make_instance(module_name, class_name, items).data)


class JResponse(HttpResponse):
    """for simple dict response, like success and failed, etc"""

    def __init__(self, result, status=200, *args, **kwargs):
        if not isinstance(result, list):
            if 'errors' in result.keys():
                dt = result.pop('errors', {}) or {}
                result['msg'] = ''.join([e[0] for e in dt.values()])
        super(JResponse, self).__init__(
            json.dumps(result), status=status, mimetype='application/json', *args, **kwargs
        )


class Response(RfResponse):
    """for object json response"""

    def __init__(self, data, *args, **kwargs):
        if isinstance(data, dict) and 'code' in data.keys(): # data为dict, 且已有code则无需再添加code返回
            super(Response, self).__init__(data, *args, **kwargs)
        else:
            super(Response, self).__init__(codes.append('ok', {'data': data}), *args, **kwargs)