#coding=utf-8

"""
自动部署工具
"""

from fabric.api import cd, run, env, settings, task, sudo
from fabric.colors import *

# 远程服务器
env.hosts = ['ada@115.29.210.179:54803']


@task
def update_server():
    """ 更新服务器(pull_code, restart_fish, ) """
    print green('Start update_server...')
    with settings(warn_only=True):
        with cd('/home/ada/prod/fish'):
            run('git pull origin master')
        sudo('supervisorctl restart fish')
        # run('tail -100f /home/ada/prod/fish/logs/fish.log')
    print green('End update_server')

@task
def flush_redis():
    """ Call redis-cli flushall """
    run('redis-cli flushall')

@task
def log_host():
    """ tail fish.log """
    run('tail -100f /home/ada/prod/fish/logs/fish.log')